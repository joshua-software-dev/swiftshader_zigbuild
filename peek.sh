#!/bin/bash

set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

ls -l zig-cache/h/ | sort -n | tail -n 11 | head -n 10
