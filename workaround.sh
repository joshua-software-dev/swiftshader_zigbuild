#!/bin/bash

set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

for i in $(ls -l zig-cache/h/ | sort -n | tail -n 2 | head -n 1 | awk '{print $9}'); do
    rm "zig-cache/h/$i"
done

for i in $(ls -l .zig-cache/h/ | sort -n | tail -n 2 | head -n 1 | awk '{print $9}'); do
    rm ".zig-cache/h/$i"
done
