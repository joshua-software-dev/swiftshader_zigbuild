# swiftshader_zigbuild

[SwiftShader](https://swiftshader.googlesource.com/SwiftShader) packaged for the zig build system

# JIT Modes

SwiftShader supports different JIT engines, the three packaged with this build script are:

`LLVMv10` `LLVMv16` `Subzero`

You can choose your preferred engine with the `jit_mode` compile option:
```
zig build -Djit_mode=LLVMv16
```
or in your `build.zig`:
```zig
pub fn build(b: *std.Build) void {
    // ...

    const swiftshader_dep = b.dependency("swiftshader_zigbuild", .{
        .target = target,
        .optimize = optimize,
        .jit_mode = @as([]const u8, "LLVMv16"),
    });

    // for static linking
    const static_lib = swiftshader_dep.artifact("vk_swiftshader_static");

    // for dynamic linking / dlopen
    const shared_lib = swiftshader_dep.artifact("vk_swiftshader");

    // ...
}
```

Different JITs have different support for different CPU architectures. At the time of writing, the support is as as follows:

### LLVMv10
`LLVMv10` is smaller than LLVMv16 and compiles slightly faster.
```
aarch64-linux-gnu
aarch64-linux-musl
arm-linux-gnueabihf
arm-linux-musleabihf
mips64el-linux-gnuabi64 // requires +xgot
mips64el-linux-musl // requires +xgot
mipsel-linux-gnueabihf
mipsel-linux-musl
x86_64-linux-gnu
x86_64-linux-musl
x86-linux-gnu
x86-linux-musl

aarch64-windows-gnu
x86_64-windows-gnu
x86-windows-gnu

aarch64-macos-none
x86_64-macos-none
```

`*-windows-msvc` support is untested.

### LLVMv16
`LLVMv16` brings in a more recent update of LLVM. This is the largest backend and takes the longest to compile. It is also the only backend with riscv64 support.
```
aarch64-linux-gnu
aarch64-linux-musl
arm-linux-gnueabihf
arm-linux-musleabihf
mips64el-linux-gnuabi64 // requires +xgot
mips64el-linux-musl // requires +xgot
mipsel-linux-gnueabihf
mipsel-linux-musl
riscv64-linux-musl
x86_64-linux-gnu
x86_64-linux-musl
x86-linux-gnu
x86-linux-musl

aarch64-windows-gnu
x86_64-windows-gnu
x86-windows-gnu

aarch64-macos-none
x86_64-macos-none
```

`*-windows-msvc` support is untested.

### Subzero
The `Subzero` backend only supports a limited number of CPU architectures. It is also the smallest of the JIT backends by a wide margin, and is the fastest to compile.
```
arm-linux-gnueabihf
arm-linux-musleabihf
mipsel-linux-gnueabihf
mipsel-linux-musl
x86_64-linux-gnu
x86_64-linux-musl
x86-linux-gnu
x86-linux-musl

x86_64-windows-gnu
x86-windows-gnu

x86_64-macos-none
```

`*-windows-msvc` support is untested.
