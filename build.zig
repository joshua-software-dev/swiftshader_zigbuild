const std = @import("std");

const JIT_Mode = enum {
    LLVMv10,
    LLVMv16,
    Subzero,
};

const SymbolVisibility = enum {
    default,
    internal,
    hidden,
    protected,
};

const WINVER: []const u8 = std.mem.trimRight(
    u8,
    // yes, swiftshader really does set its intended version to Windows XP
    std.fmt.comptimePrint(
        "0x{x}",
        .{ @intFromEnum(std.Target.Os.WindowsVersion.xp) },
    ),
    "0",
);

fn fix_llvm_windows_threading_header(b: *std.Build, input_file: std.Build.LazyPath) std.Build.LazyPath {
    // LLVM's threading mode selector relies on MSVC specific details
    // to work correctly for Windows targets. The LLVM version that zig
    // bundles (distinct from what is being compiled here) has
    // supported compiling this threading mode used on seemingly
    // all other platforms for many versions now. LLVM even chooses it
    // for Windows if you're using MSVC. There is no good reason to try
    // to use the fallback instead.
    const exe = b.addExecutable(.{
        .name = "fix_llvm_windows_threading_header",
        .root_source_file = b.path("src/fix_llvm_windows_threading_header.zig"),
        .target = b.host,
        .optimize = .Debug
    });
    const run_exe = b.addRunArtifact(exe);
    run_exe.addFileArg(input_file);
    return run_exe.addOutputFileArg("Threading.h");
}

fn fix_llvm_musl_config(b: *std.Build, input_file: std.Build.LazyPath) std.Build.LazyPath {
    // The LLVM config included with swiftshader for Linux enables many
    // glibc only features. This must be changed in order to compile for
    // musl libc without myriad errors.
    const exe = b.addExecutable(.{
        .name = "fix_llvm_musl_config",
        .root_source_file = b.path("src/fix_llvm_musl_config.zig"),
        .target = b.host,
        .optimize = .Debug
    });
    const run_exe = b.addRunArtifact(exe);
    run_exe.addFileArg(input_file);
    return run_exe.addOutputFileArg("config.h");
}

fn fix_windows_cpuidex(b: *std.Build, input_file: std.Build.LazyPath) std.Build.LazyPath {
    // For some reason, even though MinGW defines a __cpuidex intrinsic
    // replacement, it isn't found with whatever #define's swiftshader sets.
    // Luckily, they define an assembly fallback (that happens to match the
    // implementation MinGW uses as well). Not so happily, it doesn't fallback
    // correctly into it on Windows. This changes this check to allow it to
    // work, which should have no impact on non-Windows platforms.
    const exe = b.addExecutable(.{
        .name = "fix_windows_cpuidex",
        .root_source_file = b.path("src/fix_windows_cpuidex.zig"),
        .target = b.host,
        .optimize = .Debug
    });
    const run_exe = b.addRunArtifact(exe);
    run_exe.addFileArg(input_file);
    return run_exe.addOutputFileArg("CPUID.cpp");
}

fn generate_icd_json(
    b: *std.Build,
    input_file: std.Build.LazyPath,
    replacement_path: std.Build.LazyPath
) std.Build.LazyPath {
    const exe = b.addExecutable(.{
        .name = "generate_icd_json",
        .root_source_file = b.path("src/generate_icd_json.zig"),
        .target = b.host,
        .optimize = .Debug
    });
    const run_exe = b.addRunArtifact(exe);
    run_exe.addFileArg(input_file);
    const output = run_exe.addOutputFileArg("vk_swiftshader_icd.json");
    run_exe.addFileArg(replacement_path);
    return output;
}

fn include_macos_libraries(lib: *std.Build.Step.Compile, xcode_lazy_dep: ?*std.Build.Dependency) void {
    lib.headerpad_max_install_names = true;
    if (xcode_lazy_dep) |xcode_dep| {
        lib.addSystemFrameworkPath(xcode_dep.path("Frameworks/"));
        lib.addSystemIncludePath(xcode_dep.path("include/"));
        lib.addLibraryPath(xcode_dep.path("lib/"));
    }
    lib.linkFramework("Cocoa");
    lib.linkFramework("CoreFoundation");
    lib.linkFramework("IOSurface");
    lib.linkFramework("Metal");
    lib.linkFramework("QuartzCore");
}

fn build_marl(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
) !*std.Build.Step.Compile {
    const use_ucontext = target.result.cpu.arch == .mipsel and target.result.os.tag == .linux;

    const marl_asm_flags = blk: {
        var marl_asm_flags = std.ArrayList([]const u8).init(b.allocator);

        // marl prefers to not set a -std= flag for these even though they are
        // treated as C source files. This is not ideal, as it leaves the
        // potential for compiler updates to cause problems. Nevertheless, this
        // is the decision the upsteam project made, and this is merely a build
        // script, not an edict from on high.

        try marl_asm_flags.appendSlice(default_flags);
        try marl_asm_flags.appendSlice(&.{
            // "-fPIC",
            "-Werror",
            "-Wthread-safety",
        });

        break :blk marl_asm_flags;
    };
    defer marl_asm_flags.deinit();

    const marl_cpp_flags = blk: {
        var marl_cpp_flags = std.ArrayList([]const u8).init(b.allocator);

        try marl_cpp_flags.append("-std=c++11");
        try marl_cpp_flags.appendSlice(default_flags);
        try marl_cpp_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Werror",
            "-Wno-unused-parameter",
            "-Wthread-safety",
        });
        try marl_cpp_flags.appendSlice(cpp_flags);

        break :blk marl_cpp_flags;
    };
    defer marl_cpp_flags.deinit();

    const marl = b.addStaticLibrary(.{
        .name = "marl",
        .target = target,
        .optimize = optimize,
    });
    marl.linkLibCpp();
    marl.addIncludePath(swiftshader_dep.path("third_party/marl/include"));
    if (optimize != .Debug) marl.defineCMacro("NDEBUG", null);
    if (use_ucontext) marl.defineCMacro("MARL_FIBERS_USE_UCONTEXT", null);
    if (target.result.os.tag == .windows) {
        marl.defineCMacro("NOMINMAX", null);
        marl.defineCMacro("STRICT", null);
        marl.defineCMacro("WINVER", WINVER);
        // Despite the fact this project includes intrin.h where it should need
        // to, it fails to resolve the __nop() symbol defined there. Luckily,
        // the implementation is dead simple, so just use the preprocessor to
        // replace the function call with the implementation.
        marl.defineCMacro("__nop()", "__asm__ __volatile__(\"nop\")");
    }
    marl.link_data_sections = true; // "-fdata-sections",
    marl.link_function_sections = true; // "-ffunction-sections",
    marl.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    marl.root_module.pic = true; // "-fPIC",
    marl.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/marl/src/debug.cpp",
            "third_party/marl/src/memory.cpp",
            "third_party/marl/src/scheduler.cpp",
            "third_party/marl/src/thread.cpp",
            "third_party/marl/src/trace.cpp",
        },
        .flags = marl_cpp_flags.items,
    });

    if (!use_ucontext) {
        marl.addCSourceFile(.{
            .file = swiftshader_dep.path("third_party/marl/src/osfiber_emscripten.cpp"),
            .flags = marl_cpp_flags.items,
        });

        marl.addCSourceFiles(.{
            .root = swiftshader_dep.path(""),
            .files = &.{
                "third_party/marl/src/osfiber_aarch64.c",
                "third_party/marl/src/osfiber_arm.c",
                "third_party/marl/src/osfiber_asm_aarch64.S",
                "third_party/marl/src/osfiber_asm_arm.S",
                "third_party/marl/src/osfiber_asm_loongarch64.S",
                "third_party/marl/src/osfiber_asm_mips64.S",
                "third_party/marl/src/osfiber_asm_ppc64.S",
                "third_party/marl/src/osfiber_asm_rv64.S",
                "third_party/marl/src/osfiber_asm_x64.S",
                "third_party/marl/src/osfiber_asm_x86.S",
                "third_party/marl/src/osfiber_loongarch64.c",
                "third_party/marl/src/osfiber_mips64.c",
                "third_party/marl/src/osfiber_ppc64.c",
                "third_party/marl/src/osfiber_rv64.c",
                "third_party/marl/src/osfiber_x64.c",
                "third_party/marl/src/osfiber_x86.c",
            },
            .flags = marl_asm_flags.items,
        });
    }

    return marl;
}

fn build_llvm_v10(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
) !*std.Build.Step.Compile {
    const llvm_c_flags = blk: {
        var llvm_c_flags = std.ArrayList([]const u8).init(b.allocator);

        // LLVM prefers to not set a -std= flag for these even though they are
        // treated as C source files. This is not ideal, as it leaves the
        // potential for compiler updates to cause problems. Nevertheless, this
        // is the decision the upsteam project made, and this is merely a build
        // script, not an edict from on high.

        try llvm_c_flags.appendSlice(default_flags);
        try llvm_c_flags.appendSlice(&.{
            // "-fPIC",
            "-Werror",
            "-Wno-unused-parameter",
            "-Wno-deprecated-declarations",
        });

        break :blk llvm_c_flags;
    };
    defer llvm_c_flags.deinit();

    const llvm_cpp_flags = blk: {
        var llvm_cpp_flags = std.ArrayList([]const u8).init(b.allocator);

        try llvm_cpp_flags.append("-std=c++17");
        try llvm_cpp_flags.appendSlice(default_flags);
        try llvm_cpp_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Werror",
            "-Wno-bitwise-instead-of-logical",
            "-Wno-deprecated-declarations",
            "-Wno-unused-but-set-parameter",
            "-Wno-unused-but-set-variable",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wno-unused-variable",
        });

        switch (target.result.cpu.arch) {
            .arm, .mipsel, .x86 => try llvm_cpp_flags.append("-Wno-sign-compare"),
            else => {},
        }

        try llvm_cpp_flags.appendSlice(cpp_flags);

        break :blk llvm_cpp_flags;
    };
    defer llvm_cpp_flags.deinit();

    const llvm = b.addStaticLibrary(.{
        .name = "llvm_v10",
        .target = target,
        .optimize = optimize,
    });
    llvm.linkLibCpp();

    switch (target.result.os.tag) {
        .windows => {
            const @"Threading.h" = fix_llvm_windows_threading_header(
                b,
                swiftshader_dep.path("third_party/llvm-10.0/llvm/include/llvm/Support/Threading.h"),
            );

            const wf = b.addWriteFiles();
            _ = wf.addCopyFile(@"Threading.h", "llvm/Support/Threading.h");
            llvm.addIncludePath(wf.getDirectory());
        },
        else => {},
    }

    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/include"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/lib/IR"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/lib/Target/AArch64"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/lib/Target/ARM"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/lib/Target/Mips"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/lib/Target/PowerPC"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/lib/Target/RISCV"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/lib/Target/X86"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/configs/common/lib/Transforms/InstCombine"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/llvm/include"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/llvm/lib/Target/AArch64"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/llvm/lib/Target/ARM"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/llvm/lib/Target/Mips"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/llvm/lib/Target/PowerPC"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/llvm/lib/Target/RISCV"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-10.0/llvm/lib/Target/X86"));
    if (target.result.abi.isMusl()) {
        const @"config.h" = fix_llvm_musl_config(
            b,
            swiftshader_dep.path("third_party/llvm-10.0/configs/linux/include/llvm/Config/config.h"),
        );

        const wf = b.addWriteFiles();
        _ = wf.addCopyFile(@"config.h", "llvm/Config/config.h");
        llvm.addIncludePath(wf.getDirectory());
    }
    llvm.addIncludePath(swiftshader_dep.path(
        switch (target.result.os.tag) {
            .ios, .macos, .watchos, .tvos => "third_party/llvm-10.0/configs/darwin/include",
            .linux => "third_party/llvm-10.0/configs/linux/include",
            .windows => "third_party/llvm-10.0/configs/windows/include",
            else => return error.UnsupportedOS,
        },
    ));
    if (optimize != .Debug) llvm.defineCMacro("NDEBUG", null);
    llvm.defineCMacro("__STDC_CONSTANT_MACROS", null);
    llvm.defineCMacro("__STDC_LIMIT_MACROS", null);
    if (target.result.os.tag == .windows) {
        llvm.defineCMacro("NOMINMAX", null);
        llvm.defineCMacro("STRICT", null);
        llvm.defineCMacro("WINVER", WINVER);

        // LLVM uses MSVC specific checks here to determine where to grab the
        // c++ ::write function from. For MinGW, it doesn't even consider using
        // <unistd.h>, so we must manually enable the flag to use it.
        if (target.result.abi.isGnu()) {
            llvm.defineCMacro("HAVE_UNISTD_H", null);
        }
    }
    llvm.link_data_sections = true; // "-fdata-sections",
    llvm.link_function_sections = true; // "-ffunction-sections",
    llvm.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    llvm.root_module.pic = true; // "-fPIC",
    llvm.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/llvm-10.0/llvm/lib/Analysis/AliasAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/AliasAnalysisSummary.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/AliasSetTracker.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/AssumptionCache.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/BasicAliasAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/BlockFrequencyInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/BlockFrequencyInfoImpl.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/BranchProbabilityInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CFG.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CFLAndersAliasAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CFLSteensAliasAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CGSCCPassManager.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CallGraph.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CallGraphSCCPass.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CaptureTracking.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CmpInstAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/CodeMetrics.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ConstantFolding.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/DemandedBits.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/DependenceAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/DivergenceAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/DomTreeUpdater.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/DominanceFrontier.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/EHPersonalities.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/GlobalsModRef.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/GuardUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/IVDescriptors.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/IVUsers.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/IndirectCallPromotionAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/InlineCost.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/InstructionPrecedenceTracking.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/InstructionSimplify.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LazyBlockFrequencyInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LazyBranchProbabilityInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LazyCallGraph.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LazyValueInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LegacyDivergenceAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/Loads.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LoopAccessAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LoopAnalysisManager.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LoopInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LoopPass.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/LoopUnrollAnalyzer.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/MemoryBuiltins.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/MemoryDependenceAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/MemoryLocation.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/MemorySSA.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/MemorySSAUpdater.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ModuleSummaryAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/MustExecute.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ObjCARCAliasAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ObjCARCAnalysisUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ObjCARCInstKind.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/OptimizationRemarkEmitter.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/OrderedBasicBlock.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/OrderedInstructions.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/PHITransAddr.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/PhiValues.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/PostDominators.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ProfileSummaryInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/PtrUseVisitor.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/RegionInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/RegionPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ScalarEvolution.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ScalarEvolutionAliasAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ScalarEvolutionExpander.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ScalarEvolutionNormalization.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ScopedNoAliasAA.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/SyncDependenceAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/TargetLibraryInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/TargetTransformInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/TypeBasedAliasAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/TypeMetadataUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/VFABIDemangling.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ValueLattice.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ValueLatticeUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/ValueTracking.cpp",
            "third_party/llvm-10.0/llvm/lib/Analysis/VectorUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/BinaryFormat/Dwarf.cpp",
            "third_party/llvm-10.0/llvm/lib/BinaryFormat/Magic.cpp",
            "third_party/llvm-10.0/llvm/lib/BinaryFormat/Wasm.cpp",
            "third_party/llvm-10.0/llvm/lib/BinaryFormat/XCOFF.cpp",
            "third_party/llvm-10.0/llvm/lib/Bitcode/Reader/BitcodeReader.cpp",
            "third_party/llvm-10.0/llvm/lib/Bitcode/Reader/MetadataLoader.cpp",
            "third_party/llvm-10.0/llvm/lib/Bitcode/Reader/ValueList.cpp",
            "third_party/llvm-10.0/llvm/lib/Bitcode/Writer/BitcodeWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/Bitcode/Writer/ValueEnumerator.cpp",
            "third_party/llvm-10.0/llvm/lib/Bitstream/Reader/BitstreamReader.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AggressiveAntiDepBreaker.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AllocationOrder.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/Analysis.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/ARMException.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/AccelTable.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/AddressPool.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/AsmPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/AsmPrinterDwarf.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/AsmPrinterInlineAsm.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/CodeViewDebug.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DIE.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DIEHash.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DbgEntityHistoryCalculator.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DebugHandlerBase.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DebugLocStream.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DwarfCFIException.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DwarfCompileUnit.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DwarfDebug.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DwarfExpression.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DwarfFile.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DwarfStringPool.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/DwarfUnit.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/EHStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/WasmException.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/WinCFGuard.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AsmPrinter/WinException.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/AtomicExpandPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/BasicTargetTransformInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/BranchFolding.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/BranchRelaxation.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/BreakFalseDeps.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/CFGuardLongjmp.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/CFIInstrInserter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/CalcSpillWeights.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/CallingConvLower.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/CodeGen.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/CodeGenPrepare.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/CriticalAntiDepBreaker.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/DFAPacketizer.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/DeadMachineInstructionElim.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/DetectDeadLanes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/DwarfEHPrepare.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/EarlyIfConversion.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/EdgeBundles.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ExecutionDomainFix.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ExpandMemCmp.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ExpandPostRAPseudos.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ExpandReductions.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/FEntryInserter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/FaultMaps.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/FinalizeISel.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/FuncletLayout.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GCMetadata.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GCMetadataPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GCRootLowering.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GCStrategy.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/CSEInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/CSEMIRBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/CallLowering.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/Combiner.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/CombinerHelper.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/GISelChangeObserver.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/GISelKnownBits.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/GlobalISel.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/IRTranslator.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/InstructionSelect.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/InstructionSelector.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/LegalityPredicates.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/LegalizeMutations.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/Legalizer.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/LegalizerHelper.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/LegalizerInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/Localizer.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/MachineIRBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/RegBankSelect.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/RegisterBank.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/RegisterBankInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalISel/Utils.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/GlobalMerge.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/HardwareLoops.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/IfConversion.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ImplicitNullChecks.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/IndirectBrExpandPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/InlineSpiller.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/InterferenceCache.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/InterleavedAccessPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/InterleavedLoadCombinePass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/IntrinsicLowering.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LLVMTargetMachine.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LatencyPriorityQueue.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LazyMachineBlockFrequencyInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LexicalScopes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveDebugValues.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveDebugVariables.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveInterval.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveIntervalUnion.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveIntervals.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LivePhysRegs.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveRangeCalc.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveRangeEdit.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveRangeShrink.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveRegMatrix.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveRegUnits.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveStacks.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LiveVariables.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LocalStackSlotAllocation.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LoopTraversal.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LowLevelType.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/LowerEmuTLS.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MIRCanonicalizerPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MIRNamerPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MIRPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MIRPrintingPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MIRVRegNamerUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineBasicBlock.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineBlockFrequencyInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineBlockPlacement.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineBranchProbabilityInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineCSE.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineCombiner.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineCopyPropagation.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineDominanceFrontier.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineDominators.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineFrameInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineFunction.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineFunctionPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineFunctionPrinterPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineInstr.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineInstrBundle.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineLICM.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineLoopInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineLoopUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineModuleInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineModuleInfoImpls.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineOperand.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineOptimizationRemarkEmitter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineOutliner.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachinePipeliner.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachinePostDominators.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineRegionInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineRegisterInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineSSAUpdater.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineScheduler.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineSink.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineSizeOpts.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineTraceMetrics.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MachineVerifier.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/MacroFusion.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ModuloSchedule.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/OptimizePHIs.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PHIElimination.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PHIEliminationUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PatchableFunction.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PeepholeOptimizer.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PostRAHazardRecognizer.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PostRASchedulerList.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PreISelIntrinsicLowering.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ProcessImplicitDefs.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PrologEpilogInserter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/PseudoSourceValue.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ReachingDefAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegAllocBase.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegAllocBasic.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegAllocFast.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegAllocGreedy.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegAllocPBQP.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegUsageInfoCollector.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegUsageInfoPropagate.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegisterClassInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegisterCoalescer.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegisterPressure.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegisterScavenging.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RegisterUsageInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/RenameIndependentSubregs.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ResetMachineFunctionPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SafeStack.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SafeStackColoring.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SafeStackLayout.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ScalarizeMaskedMemIntrin.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ScheduleDAG.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ScheduleDAGInstrs.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ScheduleDAGPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ScoreboardHazardRecognizer.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/DAGCombiner.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/FastISel.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/FunctionLoweringInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/InstrEmitter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/LegalizeDAG.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/LegalizeFloatTypes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/LegalizeIntegerTypes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/LegalizeTypes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/LegalizeTypesGeneric.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/LegalizeVectorOps.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/LegalizeVectorTypes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/ResourcePriorityQueue.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/ScheduleDAGRRList.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/ScheduleDAGSDNodes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/ScheduleDAGVLIW.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAG.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGAddressAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGDumper.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGISel.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGTargetInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/StatepointLowering.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SelectionDAG/TargetLowering.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ShadowStackGCLowering.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ShrinkWrap.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SjLjEHPrepare.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SlotIndexes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SpillPlacement.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SplitKit.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/StackColoring.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/StackMapLivenessAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/StackMaps.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/StackProtector.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/StackSlotColoring.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SwiftErrorValueTracking.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/SwitchLoweringUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TailDuplication.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TailDuplicator.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetFrameLoweringImpl.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetInstrInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetLoweringBase.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetLoweringObjectFileImpl.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetOptionsImpl.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetPassConfig.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetRegisterInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetSchedule.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TargetSubtargetInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TwoAddressInstructionPass.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/TypePromotion.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/UnreachableBlockElim.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/ValueTypes.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/VirtRegMap.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/WasmEHPrepare.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/WinEHPrepare.cpp",
            "third_party/llvm-10.0/llvm/lib/CodeGen/XRayInstrumentation.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/CVTypeVisitor.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/CodeViewError.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/CodeViewRecordIO.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/ContinuationRecordBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/EnumTables.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/GlobalTypeTableBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/Line.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/RecordName.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/RecordSerialization.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/SimpleTypeSerializer.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/SymbolRecordMapping.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/TypeHashing.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/TypeIndex.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/TypeIndexDiscovery.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/TypeRecordMapping.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/CodeView/TypeTableCollection.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFAbbreviationDeclaration.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFAcceleratorTable.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFAddressRange.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFCompileUnit.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFContext.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDataExtractor.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugAbbrev.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugAddr.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugArangeSet.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugAranges.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugFrame.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugInfoEntry.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugLine.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugLoc.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugMacro.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugPubTable.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugRangeList.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDebugRnglists.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFDie.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFExpression.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFFormValue.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFGdbIndex.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFListTable.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFTypeUnit.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFUnit.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFUnitIndex.cpp",
            "third_party/llvm-10.0/llvm/lib/DebugInfo/DWARF/DWARFVerifier.cpp",
            "third_party/llvm-10.0/llvm/lib/Demangle/ItaniumDemangle.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/ExecutionEngine.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/GDBRegistrationListener.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/Orc/CompileUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/Orc/Core.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/Orc/IRCompileLayer.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/Orc/JITTargetMachineBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/Orc/Layer.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/Orc/Legacy.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/Orc/ThreadSafeModule.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/OrcError/OrcError.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/RuntimeDyld/JITSymbol.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/RuntimeDyld/RTDyldMemoryManager.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/RuntimeDyld/RuntimeDyld.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/RuntimeDyld/RuntimeDyldCOFF.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/RuntimeDyld/RuntimeDyldELF.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/RuntimeDyld/RuntimeDyldMachO.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/RuntimeDyld/Targets/RuntimeDyldELFMips.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/SectionMemoryManager.cpp",
            "third_party/llvm-10.0/llvm/lib/ExecutionEngine/TargetSelect.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/AbstractCallSite.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/AsmWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Attributes.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/AutoUpgrade.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/BasicBlock.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Comdat.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/ConstantFold.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/ConstantRange.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Constants.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/DIBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/DataLayout.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/DebugInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/DebugInfoMetadata.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/DebugLoc.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/DiagnosticHandler.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/DiagnosticInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/DiagnosticPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Dominators.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/FPEnv.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Function.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/GVMaterializer.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Globals.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/IRBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/IRPrintingPasses.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/InlineAsm.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Instruction.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Instructions.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/IntrinsicInst.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/LLVMContext.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/LLVMContextImpl.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/LegacyPassManager.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/MDBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Mangler.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Metadata.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Module.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/ModuleSummaryIndex.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Operator.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/OptBisect.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Pass.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/PassInstrumentation.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/PassManager.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/PassRegistry.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/PassTimingInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/ProfileSummary.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/RemarkStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Statepoint.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Type.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/TypeFinder.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Use.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/User.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Value.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/ValueSymbolTable.cpp",
            "third_party/llvm-10.0/llvm/lib/IR/Verifier.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/ConstantPools.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/ELFObjectWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCAsmBackend.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCAsmInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCAsmInfoCOFF.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCAsmInfoDarwin.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCAsmInfoELF.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCAsmMacro.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCAsmStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCAssembler.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCCodeEmitter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCCodeView.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCContext.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCDisassembler/MCRelocationInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCDwarf.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCELFObjectTargetWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCELFStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCExpr.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCFragment.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCInst.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCInstPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCInstrAnalysis.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCInstrDesc.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCLinkerOptimizationHint.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCMachOStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCMachObjectTargetWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCNullStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCObjectFileInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCObjectStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCObjectWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/AsmLexer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/AsmParser.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/COFFAsmParser.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/DarwinAsmParser.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/ELFAsmParser.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/MCAsmLexer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/MCAsmParser.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/MCAsmParserExtension.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/MCTargetAsmParser.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCParser/WasmAsmParser.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCRegisterInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSchedule.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSection.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSectionCOFF.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSectionELF.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSectionMachO.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSectionWasm.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSectionXCOFF.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSubtargetInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSymbol.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCSymbolELF.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCTargetOptions.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCValue.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCWasmStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCWin64EH.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCWinCOFFStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCWinEH.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MCXCOFFStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/MachObjectWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/StringTableBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/SubtargetFeature.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/WasmObjectWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/WinCOFFObjectWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/MC/XCOFFObjectWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/Archive.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/Binary.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/COFFObjectFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/Decompressor.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/ELF.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/ELFObjectFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/Error.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/IRObjectFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/IRSymtab.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/MachOObjectFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/MachOUniversal.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/Minidump.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/ModuleSymbolTable.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/ObjectFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/RecordStreamer.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/RelocationResolver.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/SymbolicFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/TapiFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/TapiUniversal.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/WasmObjectFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/WindowsResource.cpp",
            "third_party/llvm-10.0/llvm/lib/Object/XCOFFObjectFile.cpp",
            "third_party/llvm-10.0/llvm/lib/ProfileData/InstrProf.cpp",
            "third_party/llvm-10.0/llvm/lib/ProfileData/InstrProfReader.cpp",
            "third_party/llvm-10.0/llvm/lib/ProfileData/ProfileSummaryBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/ProfileData/SampleProf.cpp",
            "third_party/llvm-10.0/llvm/lib/ProfileData/SampleProfReader.cpp",
            "third_party/llvm-10.0/llvm/lib/Remarks/BitstreamRemarkParser.cpp",
            "third_party/llvm-10.0/llvm/lib/Remarks/BitstreamRemarkSerializer.cpp",
            "third_party/llvm-10.0/llvm/lib/Remarks/RemarkFormat.cpp",
            "third_party/llvm-10.0/llvm/lib/Remarks/RemarkParser.cpp",
            "third_party/llvm-10.0/llvm/lib/Remarks/RemarkSerializer.cpp",
            "third_party/llvm-10.0/llvm/lib/Remarks/RemarkStringTable.cpp",
            "third_party/llvm-10.0/llvm/lib/Remarks/YAMLRemarkParser.cpp",
            "third_party/llvm-10.0/llvm/lib/Remarks/YAMLRemarkSerializer.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ABIBreak.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/APFloat.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/APInt.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ARMAttributeParser.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ARMBuildAttrs.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ARMTargetParser.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/BinaryStreamError.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/BinaryStreamReader.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/BinaryStreamRef.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/BinaryStreamWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/BlockFrequency.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/BranchProbability.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/CRC.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Chrono.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/CodeGenCoverage.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/CommandLine.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Compression.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ConvertUTF.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ConvertUTFWrapper.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/CrashRecoveryContext.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/DJB.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/DataExtractor.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Debug.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/DebugCounter.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/DynamicLibrary.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Errno.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Error.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ErrorHandling.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/FoldingSet.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/FormatVariadic.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/FormattedStream.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/GraphWriter.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Hashing.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Host.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/IntEqClasses.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/IntervalMap.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ItaniumManglingCanonicalizer.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/JSON.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/KnownBits.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/LEB128.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/LineIterator.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Locale.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/LowLevelType.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/MD5.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ManagedStatic.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/MathExtras.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Memory.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/MemoryBuffer.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/NativeFormatting.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Optional.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Path.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/PrettyStackTrace.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Process.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Program.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/RandomNumberGenerator.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Regex.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/SHA1.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ScaledNumber.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ScopedPrinter.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Signals.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Signposts.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/SmallPtrSet.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/SmallVector.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/SourceMgr.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/SpecialCaseList.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Statistic.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/StringExtras.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/StringMap.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/StringRef.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/StringSaver.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/SymbolRemappingReader.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/TargetRegistry.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ThreadLocal.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Threading.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/TimeProfiler.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Timer.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/ToolOutputFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/TrigramIndex.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Triple.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Twine.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Unicode.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/UnicodeCaseFold.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Valgrind.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/VirtualFileSystem.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/Watchdog.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/WithColor.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/YAMLParser.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/YAMLTraits.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/circular_raw_ostream.cpp",
            "third_party/llvm-10.0/llvm/lib/Support/raw_ostream.cpp",
            "third_party/llvm-10.0/llvm/lib/Target/TargetLoweringObjectFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Target/TargetMachine.cpp",
            "third_party/llvm-10.0/llvm/lib/TextAPI/MachO/Architecture.cpp",
            "third_party/llvm-10.0/llvm/lib/TextAPI/MachO/ArchitectureSet.cpp",
            "third_party/llvm-10.0/llvm/lib/TextAPI/MachO/InterfaceFile.cpp",
            "third_party/llvm-10.0/llvm/lib/TextAPI/MachO/PackedVersion.cpp",
            "third_party/llvm-10.0/llvm/lib/TextAPI/MachO/Platform.cpp",
            "third_party/llvm-10.0/llvm/lib/TextAPI/MachO/Target.cpp",
            "third_party/llvm-10.0/llvm/lib/TextAPI/MachO/TextStub.cpp",
            "third_party/llvm-10.0/llvm/lib/TextAPI/MachO/TextStubCommon.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/AggressiveInstCombine/AggressiveInstCombine.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/AggressiveInstCombine/TruncInstCombine.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/CFGuard/CFGuard.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Coroutines/CoroCleanup.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Coroutines/CoroEarly.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Coroutines/CoroElide.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Coroutines/CoroFrame.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Coroutines/CoroSplit.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Coroutines/Coroutines.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/ArgumentPromotion.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/Attributor.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/BarrierNoopPass.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/CalledValuePropagation.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/ConstantMerge.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/CrossDSOCFI.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/DeadArgumentElimination.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/ElimAvailExtern.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/ForceFunctionAttrs.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/FunctionAttrs.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/GlobalDCE.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/GlobalOpt.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/GlobalSplit.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/HotColdSplitting.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/InferFunctionAttrs.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/InlineSimple.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/Inliner.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/LowerTypeTests.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/MergeFunctions.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/PartialInlining.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/PassManagerBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/PruneEH.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/SCCP.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/SampleProfile.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/StripDeadPrototypes.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/IPO/WholeProgramDevirt.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineAddSub.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineAndOrXor.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineAtomicRMW.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineCalls.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineCasts.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineCompares.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineLoadStoreAlloca.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineMulDivRem.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombinePHI.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineSelect.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineShifts.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineSimplifyDemanded.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstCombineVectorOps.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/InstCombine/InstructionCombining.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/AddressSanitizer.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/BoundsChecking.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/ControlHeightReduction.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/DataFlowSanitizer.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/GCOVProfiling.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/HWAddressSanitizer.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/IndirectCallPromotion.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/InstrOrderFile.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/InstrProfiling.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/Instrumentation.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/MemorySanitizer.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/PGOInstrumentation.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/PGOMemOPSizeOpt.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/SanitizerCoverage.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/ThreadSanitizer.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Instrumentation/ValueProfileCollector.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/ADCE.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/AlignmentFromAssumptions.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/BDCE.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/CallSiteSplitting.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/ConstantHoisting.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/CorrelatedValuePropagation.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/DeadStoreElimination.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/DivRemPairs.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/EarlyCSE.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/Float2Int.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/GVN.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/GVNHoist.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/GVNSink.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/IndVarSimplify.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/InstSimplifyPass.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/JumpThreading.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LICM.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopDataPrefetch.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopDeletion.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopDistribute.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopIdiomRecognize.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopInstSimplify.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopInterchange.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopLoadElimination.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopRerollPass.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopRotation.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopSimplifyCFG.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopSink.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopStrengthReduce.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopUnrollAndJamPass.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopUnrollPass.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopUnswitch.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LoopVersioningLICM.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LowerAtomic.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LowerConstantIntrinsics.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LowerExpectIntrinsic.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/LowerMatrixIntrinsics.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/MemCpyOptimizer.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/MergeICmps.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/MergedLoadStoreMotion.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/NewGVN.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/PartiallyInlineLibCalls.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/Reassociate.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/SCCP.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/SROA.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/SeparateConstOffsetFromGEP.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/SimpleLoopUnswitch.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/SimplifyCFGPass.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/SpeculativeExecution.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/TailRecursionElimination.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Scalar/WarnMissedTransforms.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/ASanStackFrameLayout.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/BasicBlockUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/BreakCriticalEdges.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/BuildLibCalls.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/BypassSlowDivision.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/CallPromotionUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/CanonicalizeAliases.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/CloneFunction.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/CloneModule.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/CodeExtractor.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/CtorUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/DemoteRegToStack.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/EntryExitInstrumenter.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/EscapeEnumerator.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/Evaluator.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/FunctionComparator.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/GlobalStatus.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/ImportedFunctionsInliningStatistics.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/InlineFunction.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LCSSA.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LibCallsShrinkWrap.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/Local.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LoopRotationUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LoopSimplify.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LoopUnroll.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LoopUnrollAndJam.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LoopUnrollPeel.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LoopUnrollRuntime.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LoopUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LoopVersioning.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/LowerInvoke.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/Mem2Reg.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/MisExpect.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/ModuleUtils.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/NameAnonGlobals.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/PredicateInfo.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/PromoteMemoryToRegister.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/SSAUpdater.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/SimplifyCFG.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/SimplifyIndVar.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/SimplifyLibCalls.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/SizeOpts.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/SymbolRewriter.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/VNCoercion.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Utils/ValueMapper.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Vectorize/LoopVectorizationLegality.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Vectorize/LoopVectorize.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Vectorize/SLPVectorizer.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Vectorize/VPlan.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Vectorize/VPlanHCFGBuilder.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Vectorize/VPlanPredicator.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Vectorize/VPlanTransforms.cpp",
            "third_party/llvm-10.0/llvm/lib/Transforms/Vectorize/VPlanVerifier.cpp",
        },
        .flags = llvm_cpp_flags.items,
    });
    llvm.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/llvm-10.0/llvm/lib/Support/regcomp.c",
            "third_party/llvm-10.0/llvm/lib/Support/regerror.c",
            "third_party/llvm-10.0/llvm/lib/Support/regexec.c",
            "third_party/llvm-10.0/llvm/lib/Support/regfree.c",
            "third_party/llvm-10.0/llvm/lib/Support/regstrlcpy.c",
        },
        .flags = llvm_c_flags.items,
    });
    llvm.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = switch (target.result.cpu.arch) {
            .arm => &.{
                "third_party/llvm-10.0/llvm/lib/Target/ARM/A15SDOptimizer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMAsmPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMBaseInstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMBaseRegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMBasicBlockInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMCallLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMCallingConv.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMConstantIslandPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMConstantPoolValue.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMExpandPseudoInsts.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMFastISel.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMFrameLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMHazardRecognizer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMISelDAGToDAG.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMISelLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMInstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMInstructionSelector.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMLegalizerInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMLoadStoreOptimizer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMLowOverheadLoops.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMMCInstLower.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMMachineFunctionInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMMacroFusion.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMOptimizeBarriersPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMParallelDSP.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMRegisterBankInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMRegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMSelectionDAGInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMSubtarget.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMTargetMachine.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMTargetObjectFile.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ARMTargetTransformInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/AsmParser/ARMAsmParser.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/Disassembler/ARMDisassembler.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMAsmBackend.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMELFObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMELFStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMInstPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMCAsmInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMCCodeEmitter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMCExpr.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMCTargetDesc.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMachORelocationInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMachObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMTargetStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMUnwindOpAsm.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMWinCOFFObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MCTargetDesc/ARMWinCOFFStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MLxExpansionPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MVEGatherScatterLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MVETailPredication.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/MVEVPTBlockPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/TargetInfo/ARMTargetInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/Thumb1FrameLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/Thumb1InstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/Thumb2ITBlockPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/Thumb2InstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/Thumb2SizeReduction.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/ThumbRegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/ARM/Utils/ARMBaseInfo.cpp",
            },
            .aarch64 => &.{
                "third_party/llvm-10.0/llvm/lib/Support/AArch64TargetParser.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64A53Fix835769.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64A57FPLoadBalancing.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64AdvSIMDScalarPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64AsmPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64BranchTargets.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64CallLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64CallingConvention.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64CleanupLocalDynamicTLSPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64CollectLOH.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64CompressJumpTables.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64CondBrTuning.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64ConditionOptimizer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64ConditionalCompares.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64DeadRegisterDefinitionsPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64ExpandImm.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64ExpandPseudoInsts.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64FalkorHWPFFix.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64FastISel.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64FrameLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64ISelDAGToDAG.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64ISelLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64InstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64InstructionSelector.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64LegalizerInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64LoadStoreOptimizer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64MCInstLower.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64MacroFusion.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64PBQPRegAlloc.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64PreLegalizerCombiner.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64PromoteConstant.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64RedundantCopyElimination.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64RegisterBankInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64RegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64SIMDInstrOpt.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64SelectionDAGInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64SpeculationHardening.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64StackTagging.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64StackTaggingPreRA.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64StorePairSuppress.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64Subtarget.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64TargetMachine.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64TargetObjectFile.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AArch64TargetTransformInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/AsmParser/AArch64AsmParser.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/Disassembler/AArch64Disassembler.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/Disassembler/AArch64ExternalSymbolizer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64AsmBackend.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64ELFObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64ELFStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64InstPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MCAsmInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MCCodeEmitter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MCExpr.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MCTargetDesc.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MachObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64TargetStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64WinCOFFObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64WinCOFFStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/TargetInfo/AArch64TargetInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/AArch64/Utils/AArch64BaseInfo.cpp",
            },
            .mipsel, .mips64el => &.{
                "third_party/llvm-10.0/llvm/lib/Target/Mips/AsmParser/MipsAsmParser.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/Disassembler/MipsDisassembler.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsABIFlagsSection.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsABIInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsAsmBackend.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsELFObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsELFStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsInstPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsMCAsmInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsMCCodeEmitter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsMCExpr.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsMCTargetDesc.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsNaClELFStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsOptionRecord.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MCTargetDesc/MipsTargetStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MicroMipsSizeReduction.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/Mips16FrameLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/Mips16HardFloat.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/Mips16HardFloatInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/Mips16InstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/Mips16ISelDAGToDAG.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/Mips16ISelLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/Mips16RegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsAnalyzeImmediate.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsAsmPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsBranchExpansion.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsCallLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsCCState.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsConstantIslandPass.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsDelaySlotFiller.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsExpandPseudo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsFastISel.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsFrameLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsInstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsInstructionSelector.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsISelDAGToDAG.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsISelLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsLegalizerInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsMachineFunction.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsMCInstLower.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsModuleISelDAGToDAG.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsOptimizePICCall.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsOs16.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsPreLegalizerCombiner.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsRegisterBankInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsRegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsSEFrameLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsSEInstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsSEISelDAGToDAG.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsSEISelLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsSERegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsSubtarget.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsTargetMachine.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/MipsTargetObjectFile.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/Mips/TargetInfo/MipsTargetInfo.cpp",
            },
            .riscv64 => &.{
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/AsmParser/RISCVAsmParser.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/Disassembler/RISCVDisassembler.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVAsmBackend.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVELFObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVELFStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVInstPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCAsmInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCCodeEmitter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCExpr.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCTargetDesc.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVTargetStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVAsmPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVCallLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVExpandPseudoInsts.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVFrameLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVInstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVInstructionSelector.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVISelDAGToDAG.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVISelLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVLegalizerInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVMCInstLower.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVMergeBaseOffset.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVRegisterBankInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVRegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVSubtarget.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVTargetMachine.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVTargetObjectFile.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/RISCVTargetTransformInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/TargetInfo/RISCVTargetInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/Utils/RISCVBaseInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/RISCV/Utils/RISCVMatInt.cpp",
            },
            .x86, .x86_64 => &.{
                "third_party/llvm-10.0/llvm/lib/Target/X86/AsmParser/X86AsmParser.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86ATTInstPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86AsmBackend.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86ELFObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86InstComments.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86InstPrinterCommon.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86IntelInstPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86MCAsmInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86MCCodeEmitter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86MCTargetDesc.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86MachObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86WinCOFFObjectWriter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86WinCOFFStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/MCTargetDesc/X86WinCOFFTargetStreamer.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/TargetInfo/X86TargetInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/Utils/X86ShuffleDecode.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86AsmPrinter.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86AvoidStoreForwardingBlocks.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86AvoidTrailingCall.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86CallFrameOptimization.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86CallLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86CallingConv.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86CmovConversion.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86CondBrFolding.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86DiscriminateMemOps.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86DomainReassignment.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86EvexToVex.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86ExpandPseudo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86FastISel.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86FixupBWInsts.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86FixupLEAs.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86FixupSetCC.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86FlagsCopyLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86FloatingPoint.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86FrameLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86ISelDAGToDAG.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86ISelLowering.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86IndirectBranchTracking.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86InsertPrefetch.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86InstrFMA3Info.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86InstrFoldTables.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86InstrInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86InstructionSelector.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86InterleavedAccess.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86LegalizerInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86MCInstLower.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86MachineFunctionInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86MacroFusion.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86OptimizeLEAs.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86PadShortFunction.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86RegisterBankInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86RegisterInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86RetpolineThunks.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86SelectionDAGInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86ShuffleDecodeConstantPool.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86SpeculativeLoadHardening.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86Subtarget.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86TargetMachine.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86TargetObjectFile.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86TargetTransformInfo.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86VZeroUpper.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86WinAllocaExpander.cpp",
                "third_party/llvm-10.0/llvm/lib/Target/X86/X86WinEHState.cpp",
            },
            else => return error.UnsupportedCPUArchitecture,
        },
        .flags = llvm_cpp_flags.items,
    });

    return llvm;
}

fn build_llvm_v16(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
) !*std.Build.Step.Compile {
    const llvm_c_flags = blk: {
        var llvm_c_flags = std.ArrayList([]const u8).init(b.allocator);

        // LLVM prefers to not set a -std= flag for these even though they are
        // treated as C source files. This is not ideal, as it leaves the
        // potential for compiler updates to cause problems. Nevertheless, this
        // is the decision the upsteam project made, and this is merely a build
        // script, not an edict from on high.

        try llvm_c_flags.appendSlice(default_flags);
        try llvm_c_flags.appendSlice(&.{
            // "-fPIC",
            "-Werror",
            "-Wno-unused-parameter",
            "-Wno-deprecated-declarations",
        });

        break :blk llvm_c_flags;
    };
    defer llvm_c_flags.deinit();

    const llvm_cpp_flags = blk: {
        var llvm_cpp_flags = std.ArrayList([]const u8).init(b.allocator);

        try llvm_cpp_flags.append("-std=c++17");
        try llvm_cpp_flags.appendSlice(default_flags);
        try llvm_cpp_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Werror",
            "-Wno-deprecated-declarations",
            "-Wno-unused-parameter",
        });

        switch (target.result.cpu.arch) {
            .arm, .mipsel, .x86 => try llvm_cpp_flags.append("-Wno-sign-compare"),
            else => {},
        }

        try llvm_cpp_flags.appendSlice(cpp_flags);

        break :blk llvm_cpp_flags;
    };
    defer llvm_cpp_flags.deinit();

    const llvm = b.addStaticLibrary(.{
        .name = "llvm_v16",
        .target = target,
        .optimize = optimize,
    });
    llvm.linkLibCpp();

    switch (target.result.os.tag) {
        .windows => {
            const @"Threading.h" = fix_llvm_windows_threading_header(
                b,
                swiftshader_dep.path("third_party/llvm-16.0/llvm/include/llvm/Support/Threading.h"),
            );

            const wf = b.addWriteFiles();
            _ = wf.addCopyFile(@"Threading.h", "llvm/Support/Threading.h");
            llvm.addIncludePath(wf.getDirectory());
        },
        else => {},
    }

    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/include"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/ExecutionEngine/JITLink"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/IR"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/Target/AArch64"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/Target/ARM"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/Target/Mips"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/Target/PowerPC"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/Target/RISCV"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/Target/X86"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/configs/common/lib/Transforms/InstCombine"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/llvm/include"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/llvm/lib/Target/AArch64"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/llvm/lib/Target/ARM"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/llvm/lib/Target/Mips"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/llvm/lib/Target/PowerPC"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/llvm/lib/Target/RISCV"));
    llvm.addIncludePath(swiftshader_dep.path("third_party/llvm-16.0/llvm/lib/Target/X86"));
    if (target.result.abi.isMusl()) {
        const @"config.h" = fix_llvm_musl_config(
            b,
            swiftshader_dep.path("third_party/llvm-16.0/configs/linux/include/llvm/Config/config.h"),
        );

        const wf = b.addWriteFiles();
        _ = wf.addCopyFile(@"config.h", "llvm/Config/config.h");
        llvm.addIncludePath(wf.getDirectory());
    }
    llvm.addIncludePath(swiftshader_dep.path(
        switch (target.result.os.tag) {
            .ios, .macos, .watchos, .tvos => "third_party/llvm-16.0/configs/darwin/include",
            .linux => "third_party/llvm-16.0/configs/linux/include",
            .windows => "third_party/llvm-16.0/configs/windows/include",
            else => return error.UnsupportedOS,
        },
    ));
    llvm.defineCMacro("__STDC_CONSTANT_MACROS", null);
    llvm.defineCMacro("__STDC_LIMIT_MACROS", null);
    llvm.defineCMacro("BLAKE3_NO_AVX2", null);
    llvm.defineCMacro("BLAKE3_NO_AVX512", null);
    llvm.defineCMacro("BLAKE3_NO_SSE2", null);
    llvm.defineCMacro("BLAKE3_NO_SSE41", null);
    llvm.defineCMacro("BLAKE3_USE_NEON", "0");
    if (optimize != .Debug) llvm.defineCMacro("NDEBUG", null);
    if (target.result.os.tag == .windows) {
        llvm.defineCMacro("NOMINMAX", null);
        llvm.defineCMacro("STRICT", null);
        llvm.defineCMacro("WINVER", WINVER);

        // LLVM uses MSVC specific checks here to determine where to grab the
        // c++ ::write function from. For MinGW, it doesn't even consider using
        // <unistd.h>, so we must manually enable the flag to use it.
        if (target.result.abi.isGnu()) {
            llvm.defineCMacro("HAVE_UNISTD_H", null);
        }
    }
    llvm.link_data_sections = true; // "-fdata-sections",
    llvm.link_function_sections = true; // "-ffunction-sections",
    llvm.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    llvm.root_module.pic = true; // "-fPIC",
    llvm.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/llvm-16.0/llvm/lib/Analysis/AliasAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/AliasAnalysisEvaluator.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/AliasSetTracker.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/AssumeBundleQueries.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/AssumptionCache.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/BasicAliasAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/BlockFrequencyInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/BlockFrequencyInfoImpl.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/BranchProbabilityInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CallGraph.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CallGraphSCCPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CallPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CaptureTracking.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CFG.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CFGPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CFGSCCPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CGSCCPassManager.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CmpInstAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CodeMetrics.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ConstantFolding.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ConstraintSystem.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CostModel.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/CycleAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/DDG.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/DDGPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/Delinearization.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/DemandedBits.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/DependenceAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/DependenceGraphBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/DivergenceAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/DominanceFrontier.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/DomTreeUpdater.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/EHPersonalities.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/FunctionPropertiesAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/GlobalsModRef.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/GuardUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/HeatUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ImportedFunctionsInliningStatistics.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/IndirectCallPromotionAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/InlineAdvisor.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/InlineCost.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/InlineOrder.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/InlineSizeEstimatorAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/InstCount.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/InstructionPrecedenceTracking.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/InstructionSimplify.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/IRSimilarityIdentifier.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/IVDescriptors.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/IVUsers.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LazyBlockFrequencyInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LazyBranchProbabilityInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LazyCallGraph.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LazyValueInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LegacyDivergenceAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/Lint.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/Loads.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/Local.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LoopAccessAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LoopAnalysisManager.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LoopCacheAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LoopInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LoopNestAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LoopPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/LoopUnrollAnalyzer.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/MemDerefPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/MemoryBuiltins.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/MemoryDependenceAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/MemoryLocation.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/MemoryProfileInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/MemorySSA.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/MemorySSAUpdater.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ModuleDebugInfoPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ModuleSummaryAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/MustExecute.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ObjCARCAliasAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ObjCARCAnalysisUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ObjCARCInstKind.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/OptimizationRemarkEmitter.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/OverflowInstAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/PHITransAddr.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/PhiValues.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/PostDominators.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ProfileSummaryInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/PtrUseVisitor.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/RegionInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/RegionPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/RegionPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ReplayInlineAdvisor.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ScalarEvolution.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ScalarEvolutionAliasAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ScalarEvolutionDivision.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ScalarEvolutionNormalization.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ScopedNoAliasAA.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/StackLifetime.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/StackSafetyAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/SyncDependenceAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/SyntheticCountsUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/TargetLibraryInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/TargetTransformInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/TensorSpec.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/TypeBasedAliasAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/TypeMetadataUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/UniformityAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ValueLattice.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ValueLatticeUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/ValueTracking.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/VectorUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Analysis/VFABIDemangling.cpp",
            "third_party/llvm-16.0/llvm/lib/AsmParser/LLLexer.cpp",
            "third_party/llvm-16.0/llvm/lib/AsmParser/LLParser.cpp",
            "third_party/llvm-16.0/llvm/lib/AsmParser/Parser.cpp",
            "third_party/llvm-16.0/llvm/lib/BinaryFormat/COFF.cpp",
            "third_party/llvm-16.0/llvm/lib/BinaryFormat/Dwarf.cpp",
            "third_party/llvm-16.0/llvm/lib/BinaryFormat/MachO.cpp",
            "third_party/llvm-16.0/llvm/lib/BinaryFormat/Magic.cpp",
            "third_party/llvm-16.0/llvm/lib/BinaryFormat/Wasm.cpp",
            "third_party/llvm-16.0/llvm/lib/BinaryFormat/XCOFF.cpp",
            "third_party/llvm-16.0/llvm/lib/Bitcode/Reader/BitcodeReader.cpp",
            "third_party/llvm-16.0/llvm/lib/Bitcode/Reader/MetadataLoader.cpp",
            "third_party/llvm-16.0/llvm/lib/Bitcode/Reader/ValueList.cpp",
            "third_party/llvm-16.0/llvm/lib/Bitcode/Writer/BitcodeWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/Bitcode/Writer/ValueEnumerator.cpp",
            "third_party/llvm-16.0/llvm/lib/Bitstream/Reader/BitstreamReader.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AggressiveAntiDepBreaker.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AllocationOrder.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/Analysis.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/AccelTable.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/AddressPool.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/AIXException.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/ARMException.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/AsmPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/AsmPrinterDwarf.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/AsmPrinterInlineAsm.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/CodeViewDebug.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DbgEntityHistoryCalculator.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DebugHandlerBase.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DebugLocStream.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DIE.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DIEHash.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DwarfCFIException.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DwarfCompileUnit.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DwarfDebug.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DwarfExpression.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DwarfFile.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DwarfStringPool.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/DwarfUnit.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/EHStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/PseudoProbePrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/WasmException.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/WinCFGuard.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AsmPrinter/WinException.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AssignmentTrackingAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/AtomicExpandPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/BasicBlockSections.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/BasicBlockSectionsProfileReader.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/BasicTargetTransformInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/BranchFolding.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/BranchRelaxation.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/BreakFalseDeps.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CalcSpillWeights.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CallingConvLower.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CFGuardLongjmp.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CFIFixup.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CFIInstrInserter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CodeGen.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CodeGenCommonISel.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CodeGenPrepare.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ComplexDeinterleavingPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/CriticalAntiDepBreaker.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/DeadMachineInstructionElim.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/DetectDeadLanes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/DFAPacketizer.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/DwarfEHPrepare.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/EarlyIfConversion.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/EdgeBundles.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/EHContGuardCatchret.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ExecutionDomainFix.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ExpandLargeDivRem.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ExpandLargeFpConvert.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ExpandMemCmp.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ExpandPostRAPseudos.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ExpandReductions.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ExpandVectorPredication.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/FaultMaps.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/FEntryInserter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/FinalizeISel.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/FixupStatepointCallerSaved.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/FuncletLayout.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GCMetadata.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GCMetadataPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GCRootLowering.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/CallLowering.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/Combiner.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/CombinerHelper.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/CSEInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/CSEMIRBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/GISelChangeObserver.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/GISelKnownBits.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/GlobalISel.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/InlineAsmLowering.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/InstructionSelect.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/InstructionSelector.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/IRTranslator.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/LegacyLegalizerInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/LegalityPredicates.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/LegalizeMutations.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/Legalizer.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/LegalizerHelper.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/LegalizerInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/LoadStoreOpt.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/Localizer.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/LostDebugLocObserver.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/MachineIRBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/RegBankSelect.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalISel/Utils.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/GlobalMerge.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/HardwareLoops.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/IfConversion.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ImplicitNullChecks.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/IndirectBrExpandPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/InlineSpiller.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/InterferenceCache.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/InterleavedAccessPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/InterleavedLoadCombinePass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/IntrinsicLowering.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/JMCInstrumenter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LatencyPriorityQueue.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LazyMachineBlockFrequencyInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LexicalScopes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveDebugValues/InstrRefBasedImpl.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveDebugValues/LiveDebugValues.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveDebugValues/VarLocBasedImpl.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveDebugVariables.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveInterval.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveIntervalCalc.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveIntervals.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveIntervalUnion.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LivePhysRegs.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveRangeCalc.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveRangeEdit.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveRangeShrink.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveRegMatrix.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveRegUnits.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveStacks.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LiveVariables.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LLVMTargetMachine.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LocalStackSlotAllocation.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LoopTraversal.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LowerEmuTLS.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/LowLevelType.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineBasicBlock.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineBlockFrequencyInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineBlockPlacement.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineBranchProbabilityInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineCFGPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineCheckDebugify.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineCombiner.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineCopyPropagation.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineCSE.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineCycleAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineDebugify.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineDominanceFrontier.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineDominators.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineFrameInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineFunction.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineFunctionPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineFunctionPrinterPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineFunctionSplitter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineInstr.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineInstrBundle.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineLateInstrsCleanup.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineLICM.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineLoopInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineLoopUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineModuleInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineModuleInfoImpls.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineModuleSlotTracker.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineOperand.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineOptimizationRemarkEmitter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineOutliner.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachinePipeliner.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachinePostDominators.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineRegionInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineRegisterInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineScheduler.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineSink.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineSizeOpts.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineSSAContext.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineSSAUpdater.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineStableHash.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineStripDebug.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineTraceMetrics.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineUniformityAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MachineVerifier.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MacroFusion.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MBFIWrapper.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MIRCanonicalizerPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MIRFSDiscriminator.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MIRNamerPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MIRPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MIRPrintingPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MIRSampleProfile.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MIRVRegNamerUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/MLRegallocEvictAdvisor.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ModuloSchedule.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/OptimizePHIs.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PatchableFunction.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PeepholeOptimizer.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PHIElimination.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PHIEliminationUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PostRAHazardRecognizer.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PostRASchedulerList.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PreISelIntrinsicLowering.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ProcessImplicitDefs.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PrologEpilogInserter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PseudoProbeInserter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/PseudoSourceValue.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RDFGraph.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RDFLiveness.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RDFRegisters.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ReachingDefAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegAllocBase.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegAllocBasic.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegAllocEvictionAdvisor.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegAllocFast.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegAllocGreedy.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegAllocPBQP.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegAllocPriorityAdvisor.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegisterBank.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegisterBankInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegisterClassInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegisterCoalescer.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegisterPressure.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegisterScavenging.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegisterUsageInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegUsageInfoCollector.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RegUsageInfoPropagate.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RemoveRedundantDebugValues.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/RenameIndependentSubregs.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ReplaceWithVeclib.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ResetMachineFunctionPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SafeStack.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SafeStackLayout.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SanitizerBinaryMetadata.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ScheduleDAG.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ScheduleDAGInstrs.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ScheduleDAGPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ScoreboardHazardRecognizer.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/DAGCombiner.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/FastISel.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/FunctionLoweringInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/InstrEmitter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/LegalizeDAG.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/LegalizeFloatTypes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/LegalizeIntegerTypes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/LegalizeTypes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/LegalizeTypesGeneric.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/LegalizeVectorOps.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/LegalizeVectorTypes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/ResourcePriorityQueue.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/ScheduleDAGFast.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/ScheduleDAGRRList.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/ScheduleDAGSDNodes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/ScheduleDAGVLIW.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAG.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGAddressAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGDumper.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGISel.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/SelectionDAGTargetInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/StatepointLowering.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectionDAG/TargetLowering.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SelectOptimize.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ShadowStackGCLowering.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ShrinkWrap.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SjLjEHPrepare.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SlotIndexes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SpillPlacement.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SplitKit.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/StackColoring.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/StackFrameLayoutAnalysisPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/StackMapLivenessAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/StackMaps.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/StackProtector.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/StackSlotColoring.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SwiftErrorValueTracking.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/SwitchLoweringUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TailDuplication.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TailDuplicator.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetFrameLoweringImpl.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetInstrInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetLoweringBase.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetLoweringObjectFileImpl.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetOptionsImpl.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetPassConfig.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetRegisterInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetSchedule.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TargetSubtargetInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TwoAddressInstructionPass.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/TypePromotion.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/UnreachableBlockElim.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/ValueTypes.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/VirtRegMap.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/WasmEHPrepare.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/WinEHPrepare.cpp",
            "third_party/llvm-16.0/llvm/lib/CodeGen/XRayInstrumentation.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/CodeViewError.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/CodeViewRecordIO.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/ContinuationRecordBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/CVTypeVisitor.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/EnumTables.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/GlobalTypeTableBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/Line.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/RecordName.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/RecordSerialization.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/SimpleTypeSerializer.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/SymbolRecordMapping.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/TypeHashing.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/TypeIndex.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/TypeIndexDiscovery.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/TypeRecordMapping.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/CodeView/TypeTableCollection.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFAbbreviationDeclaration.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFAcceleratorTable.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFAddressRange.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFCompileUnit.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFContext.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDataExtractor.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugAbbrev.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugAddr.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugAranges.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugArangeSet.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugFrame.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugInfoEntry.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugLine.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugLoc.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugMacro.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugPubTable.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugRangeList.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDebugRnglists.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFDie.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFExpression.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFFormValue.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFGdbIndex.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFListTable.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFTypePrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFTypeUnit.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFUnit.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFUnitIndex.cpp",
            "third_party/llvm-16.0/llvm/lib/DebugInfo/DWARF/DWARFVerifier.cpp",
            "third_party/llvm-16.0/llvm/lib/Demangle/Demangle.cpp",
            "third_party/llvm-16.0/llvm/lib/Demangle/DLangDemangle.cpp",
            "third_party/llvm-16.0/llvm/lib/Demangle/ItaniumDemangle.cpp",
            "third_party/llvm-16.0/llvm/lib/Demangle/MicrosoftDemangle.cpp",
            "third_party/llvm-16.0/llvm/lib/Demangle/MicrosoftDemangleNodes.cpp",
            "third_party/llvm-16.0/llvm/lib/Demangle/RustDemangle.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/aarch64.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/COFF_x86_64.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/COFF.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/COFFDirectiveParser.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/COFFLinkGraphBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/DWARFRecordSectionSplitter.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/EHFrameSupport.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/ELF_aarch64.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/ELF_i386.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/ELF_loongarch.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/ELF_riscv.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/ELF_x86_64.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/ELF.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/ELFLinkGraphBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/i386.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/JITLink.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/JITLinkGeneric.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/JITLinkMemoryManager.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/loongarch.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/MachO_arm64.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/MachO_x86_64.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/MachO.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/MachOLinkGraphBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/riscv.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/JITLink/x86_64.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/CompileUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/Core.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/DebugUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/ELFNixPlatform.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/ExecutionUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/ExecutorProcessControl.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/IRCompileLayer.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/JITTargetMachineBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/Layer.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/MachOPlatform.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/Mangling.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/ObjectFileInterface.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/ObjectLinkingLayer.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/Shared/AllocationActions.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/Shared/OrcError.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/TargetProcess/RegisterEHFrames.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/TargetProcess/TargetExecutionUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/TaskDispatch.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/Orc/ThreadSafeModule.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/RuntimeDyld/JITSymbol.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/RuntimeDyld/RTDyldMemoryManager.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/RuntimeDyld/RuntimeDyld.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/RuntimeDyld/RuntimeDyldCOFF.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/RuntimeDyld/RuntimeDyldELF.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/RuntimeDyld/RuntimeDyldMachO.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/RuntimeDyld/Targets/RuntimeDyldELFMips.cpp",
            "third_party/llvm-16.0/llvm/lib/ExecutionEngine/SectionMemoryManager.cpp",
            "third_party/llvm-16.0/llvm/lib/Frontend/OpenMP/OMPIRBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/AbstractCallSite.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/AsmWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Assumptions.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Attributes.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/AutoUpgrade.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/BasicBlock.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/BuiltinGCs.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Comdat.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/ConstantFold.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/ConstantRange.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Constants.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/DataLayout.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/DebugInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/DebugInfoMetadata.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/DebugLoc.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/DiagnosticHandler.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/DiagnosticInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/DiagnosticPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/DIBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Dominators.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/FPEnv.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Function.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/GCStrategy.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Globals.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/GVMaterializer.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/InlineAsm.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Instruction.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Instructions.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/IntrinsicInst.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/IRBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/IRPrintingPasses.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/LegacyPassManager.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/LLVMContext.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/LLVMContextImpl.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/LLVMRemarkStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Mangler.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/MDBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Metadata.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Module.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/ModuleSummaryIndex.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Operator.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/OptBisect.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Pass.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/PassInstrumentation.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/PassManager.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/PassRegistry.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/PassTimingInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/PrintPasses.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/ProfDataUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/ProfileSummary.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/PseudoProbe.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/SafepointIRVerifier.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/SSAContext.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Statepoint.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Type.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/TypeFinder.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Use.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/User.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Value.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/ValueSymbolTable.cpp",
            "third_party/llvm-16.0/llvm/lib/IR/Verifier.cpp",
            "third_party/llvm-16.0/llvm/lib/IRPrinter/IRPrintingPasses.cpp",
            "third_party/llvm-16.0/llvm/lib/IRReader/IRReader.cpp",
            "third_party/llvm-16.0/llvm/lib/Linker/IRMover.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/ELFObjectWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MachObjectWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCAsmBackend.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCAsmInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCAsmInfoCOFF.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCAsmInfoDarwin.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCAsmInfoELF.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCAsmMacro.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCAsmStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCAssembler.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCCodeEmitter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCCodeView.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCContext.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCDisassembler/MCRelocationInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCDwarf.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCDXContainerStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCDXContainerWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCELFObjectTargetWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCELFStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCExpr.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCFragment.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCInst.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCInstPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCInstrAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCInstrDesc.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCLinkerOptimizationHint.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCMachObjectTargetWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCMachOStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCNullStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCObjectFileInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCObjectStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCObjectWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/AsmLexer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/AsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/COFFAsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/DarwinAsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/ELFAsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/GOFFAsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/MCAsmLexer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/MCAsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/MCAsmParserExtension.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/MCTargetAsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/WasmAsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCParser/XCOFFAsmParser.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCPseudoProbe.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCRegisterInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSchedule.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSection.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSectionCOFF.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSectionDXContainer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSectionELF.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSectionMachO.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSectionWasm.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSectionXCOFF.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSPIRVStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSubtargetInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSymbol.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSymbolELF.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCSymbolXCOFF.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCTargetOptions.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCValue.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCWasmStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCWin64EH.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCWinCOFFStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCWinEH.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/MCXCOFFStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/SPIRVObjectWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/StringTableBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/SubtargetFeature.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/TargetRegistry.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/WasmObjectWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/WinCOFFObjectWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/MC/XCOFFObjectWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/Archive.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/Binary.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/COFFObjectFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/Decompressor.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/ELF.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/ELFObjectFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/Error.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/IRObjectFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/IRSymtab.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/MachOObjectFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/MachOUniversal.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/Minidump.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/ModuleSymbolTable.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/ObjectFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/OffloadBinary.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/RecordStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/RelocationResolver.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/SymbolicFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/TapiFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/TapiUniversal.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/WasmObjectFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/WindowsResource.cpp",
            "third_party/llvm-16.0/llvm/lib/Object/XCOFFObjectFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Option/Arg.cpp",
            "third_party/llvm-16.0/llvm/lib/Option/ArgList.cpp",
            "third_party/llvm-16.0/llvm/lib/Option/Option.cpp",
            "third_party/llvm-16.0/llvm/lib/Option/OptTable.cpp",
            "third_party/llvm-16.0/llvm/lib/Passes/OptimizationLevel.cpp",
            "third_party/llvm-16.0/llvm/lib/Passes/PassBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/Passes/PassBuilderPipelines.cpp",
            "third_party/llvm-16.0/llvm/lib/ProfileData/InstrProf.cpp",
            "third_party/llvm-16.0/llvm/lib/ProfileData/InstrProfCorrelator.cpp",
            "third_party/llvm-16.0/llvm/lib/ProfileData/InstrProfReader.cpp",
            "third_party/llvm-16.0/llvm/lib/ProfileData/MemProf.cpp",
            "third_party/llvm-16.0/llvm/lib/ProfileData/ProfileSummaryBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/ProfileData/SampleProf.cpp",
            "third_party/llvm-16.0/llvm/lib/ProfileData/SampleProfReader.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/BitstreamRemarkParser.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/BitstreamRemarkSerializer.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/RemarkFormat.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/RemarkParser.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/RemarkSerializer.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/RemarkStreamer.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/RemarkStringTable.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/YAMLRemarkParser.cpp",
            "third_party/llvm-16.0/llvm/lib/Remarks/YAMLRemarkSerializer.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ABIBreak.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/APFloat.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/APInt.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/APSInt.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ARMAttributeParser.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ARMBuildAttrs.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/BinaryStreamError.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/BinaryStreamReader.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/BinaryStreamRef.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/BinaryStreamWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/BlockFrequency.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/BranchProbability.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Chrono.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/circular_raw_ostream.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/CodeGenCoverage.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/CommandLine.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Compression.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ConvertUTF.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ConvertUTFWrapper.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/CrashRecoveryContext.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/CRC.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/DataExtractor.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Debug.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/DebugCounter.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/DivisionByConstantInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/DJB.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/DynamicLibrary.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ELFAttributeParser.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ELFAttributes.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Errno.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Error.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ErrorHandling.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ExtensibleRTTI.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/FoldingSet.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/FormattedStream.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/FormatVariadic.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/GlobPattern.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/GraphWriter.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Hashing.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/InstructionCost.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/IntEqClasses.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/IntervalMap.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ItaniumManglingCanonicalizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/JSON.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/KnownBits.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/LEB128.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/LineIterator.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Locale.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/LowLevelType.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ManagedStatic.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/MathExtras.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/MD5.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/MemAlloc.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Memory.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/MemoryBuffer.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/MemoryBufferRef.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/NativeFormatting.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/OptimizedStructLayout.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Optional.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Path.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/PrettyStackTrace.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Process.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Program.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/RandomNumberGenerator.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/raw_ostream.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Regex.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/RISCVAttributeParser.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/RISCVAttributes.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/RISCVISAInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ScaledNumber.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ScopedPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/SHA1.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Signals.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Signposts.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/SmallPtrSet.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/SmallVector.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/SourceMgr.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/SpecialCaseList.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Statistic.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/StringExtras.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/StringMap.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/StringRef.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/StringSaver.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/SuffixTree.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/SymbolRemappingReader.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Threading.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/TimeProfiler.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Timer.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/ToolOutputFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/TrigramIndex.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Twine.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/TypeSize.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Unicode.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/UnicodeCaseFold.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/Valgrind.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/VersionTuple.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/VirtualFileSystem.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/WithColor.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/xxhash.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/YAMLParser.cpp",
            "third_party/llvm-16.0/llvm/lib/Support/YAMLTraits.cpp",
            "third_party/llvm-16.0/llvm/lib/Target/TargetLoweringObjectFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Target/TargetMachine.cpp",
            "third_party/llvm-16.0/llvm/lib/TargetParser/AArch64TargetParser.cpp",
            "third_party/llvm-16.0/llvm/lib/TargetParser/ARMTargetParser.cpp",
            "third_party/llvm-16.0/llvm/lib/TargetParser/ARMTargetParserCommon.cpp",
            "third_party/llvm-16.0/llvm/lib/TargetParser/Host.cpp",
            "third_party/llvm-16.0/llvm/lib/TargetParser/Triple.cpp",
            "third_party/llvm-16.0/llvm/lib/TextAPI/Architecture.cpp",
            "third_party/llvm-16.0/llvm/lib/TextAPI/ArchitectureSet.cpp",
            "third_party/llvm-16.0/llvm/lib/TextAPI/InterfaceFile.cpp",
            "third_party/llvm-16.0/llvm/lib/TextAPI/PackedVersion.cpp",
            "third_party/llvm-16.0/llvm/lib/TextAPI/Platform.cpp",
            "third_party/llvm-16.0/llvm/lib/TextAPI/Target.cpp",
            "third_party/llvm-16.0/llvm/lib/TextAPI/TextStub.cpp",
            "third_party/llvm-16.0/llvm/lib/TextAPI/TextStubCommon.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/AggressiveInstCombine/AggressiveInstCombine.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/AggressiveInstCombine/TruncInstCombine.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/CFGuard/CFGuard.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Coroutines/CoroCleanup.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Coroutines/CoroConditionalWrapper.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Coroutines/CoroEarly.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Coroutines/CoroElide.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Coroutines/CoroFrame.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Coroutines/CoroSplit.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Coroutines/Coroutines.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineAddSub.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineAndOrXor.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineAtomicRMW.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineCalls.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineCasts.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineCompares.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineLoadStoreAlloca.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineMulDivRem.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineNegator.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombinePHI.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineSelect.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineShifts.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineSimplifyDemanded.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstCombineVectorOps.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/InstCombine/InstructionCombining.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/AddressSanitizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/BoundsChecking.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/CGProfile.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/ControlHeightReduction.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/DataFlowSanitizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/GCOVProfiling.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/HWAddressSanitizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/IndirectCallPromotion.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/InstrOrderFile.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/InstrProfiling.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/Instrumentation.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/KCFI.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/MemorySanitizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/MemProfiler.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/PGOInstrumentation.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/PGOMemOPSizeOpt.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/PoisonChecking.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/SanitizerBinaryMetadata.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/SanitizerCoverage.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/ThreadSanitizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Instrumentation/ValueProfileCollector.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/AlwaysInliner.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/Annotation2Metadata.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/ArgumentPromotion.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/Attributor.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/AttributorAttributes.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/BlockExtractor.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/CalledValuePropagation.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/ConstantMerge.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/CrossDSOCFI.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/DeadArgumentElimination.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/ElimAvailExtern.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/ForceFunctionAttrs.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/FunctionAttrs.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/FunctionImport.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/FunctionSpecialization.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/GlobalDCE.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/GlobalOpt.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/GlobalSplit.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/HotColdSplitting.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/InferFunctionAttrs.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/Inliner.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/Internalize.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/IROutliner.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/LoopExtractor.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/LowerTypeTests.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/MergeFunctions.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/ModuleInliner.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/OpenMPOpt.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/PartialInlining.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/SampleContextTracker.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/SampleProfile.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/SampleProfileProbe.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/SCCP.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/StripDeadPrototypes.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/StripSymbols.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/SyntheticCountsPropagation.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/IPO/WholeProgramDevirt.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/DependencyAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/ObjCARC.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/ObjCARCAPElim.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/ObjCARCContract.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/ObjCARCExpand.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/ObjCARCOpts.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/ProvenanceAnalysis.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/ProvenanceAnalysisEvaluator.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/ObjCARC/PtrState.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/ADCE.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/AlignmentFromAssumptions.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/AnnotationRemarks.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/BDCE.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/CallSiteSplitting.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/ConstantHoisting.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/ConstraintElimination.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/CorrelatedValuePropagation.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/DCE.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/DeadStoreElimination.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/DFAJumpThreading.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/DivRemPairs.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/EarlyCSE.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/FlattenCFGPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/Float2Int.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/GuardWidening.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/GVN.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/GVNHoist.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/GVNSink.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/InductiveRangeCheckElimination.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/IndVarSimplify.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/InferAddressSpaces.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/InstSimplifyPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/IVUsersPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/JumpThreading.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LICM.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopAccessAnalysisPrinter.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopBoundSplit.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopDataPrefetch.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopDeletion.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopDistribute.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopFlatten.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopFuse.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopIdiomRecognize.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopInstSimplify.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopInterchange.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopLoadElimination.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopPassManager.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopPredication.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopRerollPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopRotation.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopSimplifyCFG.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopSink.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopStrengthReduce.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopUnrollAndJamPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopUnrollPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LoopVersioningLICM.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LowerAtomicPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LowerConstantIntrinsics.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LowerExpectIntrinsic.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LowerGuardIntrinsic.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LowerMatrixIntrinsics.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/LowerWidenableCondition.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/MakeGuardsExplicit.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/MemCpyOptimizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/MergedLoadStoreMotion.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/MergeICmps.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/NaryReassociate.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/NewGVN.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/PartiallyInlineLibCalls.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/Reassociate.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/Reg2Mem.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/RewriteStatepointsForGC.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/ScalarizeMaskedMemIntrin.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/Scalarizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/SCCP.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/SeparateConstOffsetFromGEP.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/SimpleLoopUnswitch.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/SimplifyCFGPass.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/Sink.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/SpeculativeExecution.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/SROA.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/StraightLineStrengthReduce.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/StructurizeCFG.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/TailRecursionElimination.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/TLSVariableHoist.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Scalar/WarnMissedTransforms.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/AddDiscriminators.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/ASanStackFrameLayout.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/AssumeBundleBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/BasicBlockUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/BreakCriticalEdges.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/BuildLibCalls.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/BypassSlowDivision.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CallGraphUpdater.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CallPromotionUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CanonicalizeAliases.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CanonicalizeFreezeInLoops.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CloneFunction.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CloneModule.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CodeExtractor.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CodeLayout.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CodeMoverUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/CtorUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/Debugify.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/DemoteRegToStack.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/EntryExitInstrumenter.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/EscapeEnumerator.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/Evaluator.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/FixIrreducible.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/FlattenCFG.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/FunctionComparator.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/FunctionImportUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/GlobalStatus.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/GuardUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/HelloWorld.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/InjectTLIMappings.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/InlineFunction.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/InstructionNamer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/IntegerDivision.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LCSSA.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LibCallsShrinkWrap.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/Local.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LoopPeel.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LoopRotationUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LoopSimplify.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LoopUnroll.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LoopUnrollAndJam.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LoopUnrollRuntime.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LoopUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LoopVersioning.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LowerAtomic.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LowerGlobalDtors.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LowerIFunc.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LowerInvoke.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/LowerSwitch.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/MatrixUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/Mem2Reg.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/MemoryOpRemark.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/MemoryTaggingSupport.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/MetaRenamer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/MisExpect.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/ModuleUtils.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/NameAnonGlobals.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/PredicateInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/PromoteMemoryToRegister.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/RelLookupTableConverter.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SampleProfileInference.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SampleProfileLoaderBaseUtil.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/ScalarEvolutionExpander.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SCCPSolver.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SimplifyCFG.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SimplifyIndVar.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SimplifyLibCalls.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SizeOpts.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SSAUpdater.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SSAUpdaterBulk.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/StripGCRelocates.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/StripNonLineTableDebugInfo.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/SymbolRewriter.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/UnifyFunctionExitNodes.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/UnifyLoopExits.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/ValueMapper.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Utils/VNCoercion.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/LoadStoreVectorizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/LoopVectorizationLegality.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/LoopVectorize.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/SLPVectorizer.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/VectorCombine.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/VPlan.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/VPlanHCFGBuilder.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/VPlanRecipes.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/VPlanTransforms.cpp",
            "third_party/llvm-16.0/llvm/lib/Transforms/Vectorize/VPlanVerifier.cpp",
        },
        .flags = llvm_cpp_flags.items,
    });
    llvm.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/llvm-16.0/llvm/lib/Support/BLAKE3/blake3_dispatch.c",
            "third_party/llvm-16.0/llvm/lib/Support/BLAKE3/blake3_portable.c",
            "third_party/llvm-16.0/llvm/lib/Support/BLAKE3/blake3.c",
            "third_party/llvm-16.0/llvm/lib/Support/regcomp.c",
            "third_party/llvm-16.0/llvm/lib/Support/regerror.c",
            "third_party/llvm-16.0/llvm/lib/Support/regexec.c",
            "third_party/llvm-16.0/llvm/lib/Support/regfree.c",
            "third_party/llvm-16.0/llvm/lib/Support/regstrlcpy.c",
        },
        .flags = llvm_c_flags.items,
    });
    llvm.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = switch (target.result.cpu.arch) {
            .arm => &.{
                "third_party/llvm-16.0/llvm/lib/CodeGen/MultiHazardRecognizer.cpp",
                "third_party/llvm-16.0/llvm/lib/MC/ConstantPools.cpp",
                "third_party/llvm-16.0/llvm/lib/MC/MCInstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/A15SDOptimizer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMAsmPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMBaseInstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMBaseRegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMBasicBlockInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMBlockPlacement.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMBranchTargets.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMCallingConv.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMCallLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMConstantIslandPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMConstantPoolValue.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMExpandPseudoInsts.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMFastISel.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMFixCortexA57AES1742098Pass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMFrameLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMHazardRecognizer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMInstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMInstructionSelector.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMISelDAGToDAG.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMISelLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMLegalizerInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMLoadStoreOptimizer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMLowOverheadLoops.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMMachineFunctionInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMMacroFusion.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMMCInstLower.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMOptimizeBarriersPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMParallelDSP.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMRegisterBankInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMRegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMSelectionDAGInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMSLSHardening.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMSubtarget.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMTargetMachine.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMTargetObjectFile.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ARMTargetTransformInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/AsmParser/ARMAsmParser.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/Disassembler/ARMDisassembler.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMAsmBackend.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMELFObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMELFStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMInstPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMachObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMachORelocationInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMCAsmInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMCCodeEmitter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMCExpr.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMMCTargetDesc.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMTargetStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMUnwindOpAsm.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMWinCOFFObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MCTargetDesc/ARMWinCOFFStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MLxExpansionPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MVEGatherScatterLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MVELaneInterleavingPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MVETailPredication.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MVETPAndVPTOptimisationsPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/MVEVPTBlockPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/TargetInfo/ARMTargetInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/Thumb1FrameLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/Thumb1InstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/Thumb2InstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/Thumb2ITBlockPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/Thumb2SizeReduction.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/ThumbRegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/ARM/Utils/ARMBaseInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Transforms/IPO/BarrierNoopPass.cpp",
            },
            .aarch64 => &.{
                "third_party/llvm-16.0/llvm/lib/CodeGen/MultiHazardRecognizer.cpp",
                "third_party/llvm-16.0/llvm/lib/MC/ConstantPools.cpp",
                "third_party/llvm-16.0/llvm/lib/MC/MCInstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64A53Fix835769.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64A57FPLoadBalancing.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64AdvSIMDScalarPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64AsmPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64BranchTargets.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64CallingConvention.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64CleanupLocalDynamicTLSPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64CollectLOH.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64CompressJumpTables.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64CondBrTuning.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64ConditionalCompares.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64ConditionOptimizer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64DeadRegisterDefinitionsPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64ExpandImm.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64ExpandPseudoInsts.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64FalkorHWPFFix.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64FastISel.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64FrameLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64InstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64ISelDAGToDAG.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64ISelLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64KCFI.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64LoadStoreOptimizer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64LowerHomogeneousPrologEpilog.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64MachineFunctionInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64MachineScheduler.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64MacroFusion.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64MCInstLower.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64MIPeepholeOpt.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64PBQPRegAlloc.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64PromoteConstant.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64RedundantCopyElimination.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64RegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64SelectionDAGInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64SIMDInstrOpt.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64SLSHardening.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64SpeculationHardening.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64StackTagging.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64StackTaggingPreRA.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64StorePairSuppress.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64Subtarget.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64TargetMachine.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64TargetObjectFile.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AArch64TargetTransformInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/AsmParser/AArch64AsmParser.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/Disassembler/AArch64Disassembler.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/Disassembler/AArch64ExternalSymbolizer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64CallLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64GlobalISelUtils.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64InstructionSelector.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64LegalizerInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64O0PreLegalizerCombiner.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64PostLegalizerCombiner.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64PostLegalizerLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64PostSelectOptimize.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64PreLegalizerCombiner.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/GISel/AArch64RegisterBankInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64AsmBackend.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64ELFObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64ELFStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64InstPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MachObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MCAsmInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MCCodeEmitter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MCExpr.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64MCTargetDesc.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64TargetStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64WinCOFFObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/MCTargetDesc/AArch64WinCOFFStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/SMEABIPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/SVEIntrinsicOpts.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/TargetInfo/AArch64TargetInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/Utils/AArch64BaseInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/AArch64/Utils/AArch64SMEAttributes.cpp",
                "third_party/llvm-16.0/llvm/lib/Transforms/IPO/BarrierNoopPass.cpp",
            },
            .mipsel, .mips64el => &.{
                "third_party/llvm-16.0/llvm/lib/Target/Mips/AsmParser/MipsAsmParser.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/Disassembler/MipsDisassembler.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsABIFlagsSection.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsABIInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsAsmBackend.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsELFObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsELFStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsInstPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsMCAsmInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsMCCodeEmitter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsMCExpr.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsMCTargetDesc.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsNaClELFStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsOptionRecord.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MCTargetDesc/MipsTargetStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MicroMipsSizeReduction.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/Mips16FrameLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/Mips16HardFloat.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/Mips16HardFloatInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/Mips16InstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/Mips16ISelDAGToDAG.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/Mips16ISelLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/Mips16RegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsAnalyzeImmediate.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsAsmPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsBranchExpansion.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsCallLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsCCState.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsConstantIslandPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsDelaySlotFiller.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsExpandPseudo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsFastISel.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsFrameLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsInstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsInstructionSelector.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsISelDAGToDAG.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsISelLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsLegalizerInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsMachineFunction.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsMCInstLower.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsModuleISelDAGToDAG.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsMulMulBugPass.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsOptimizePICCall.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsOs16.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsPostLegalizerCombiner.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsPreLegalizerCombiner.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsRegisterBankInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsRegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsSEFrameLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsSEInstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsSEISelDAGToDAG.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsSEISelLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsSERegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsSubtarget.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsTargetMachine.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsTargetObjectFile.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/MipsTargetTransformInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/Mips/TargetInfo/MipsTargetInfo.cpp",
            },
            .riscv64 => &.{
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/AsmParser/RISCVAsmParser.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/Disassembler/RISCVDisassembler.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/GISel/RISCVCallLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/GISel/RISCVInstructionSelector.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/GISel/RISCVLegalizerInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/GISel/RISCVRegisterBankInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCA/RISCVCustomBehaviour.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVAsmBackend.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVBaseInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVELFObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVELFStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVInstPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCAsmInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCCodeEmitter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCExpr.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCObjectFileInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMCTargetDesc.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVMatInt.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/MCTargetDesc/RISCVTargetStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVAsmPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVCodeGenPrepare.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVExpandAtomicPseudoInsts.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVExpandPseudoInsts.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVFrameLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVGatherScatterLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVISelDAGToDAG.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVISelLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVInsertVSETVLI.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVInstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVMCInstLower.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVMachineFunctionInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVMacroFusion.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVMakeCompressible.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVMergeBaseOffset.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVRedundantCopyElimination.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVRegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVSExtWRemoval.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVStripWSuffix.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVSubtarget.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVTargetMachine.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVTargetObjectFile.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/RISCVTargetTransformInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/RISCV/TargetInfo/RISCVTargetInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/TargetParser/RISCVTargetParser.cpp",
                "third_party/llvm-16.0/llvm/lib/Transforms/IPO/BarrierNoopPass.cpp",
            },
            .x86, .x86_64 => &.{
                "third_party/llvm-16.0/llvm/lib/Target/X86/AsmParser/X86AsmParser.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/Disassembler/X86Disassembler.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCA/X86CustomBehaviour.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86AsmBackend.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86ATTInstPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86ELFObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86InstComments.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86InstPrinterCommon.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86InstrRelaxTables.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86IntelInstPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86MachObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86MCAsmInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86MCCodeEmitter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86MCTargetDesc.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86MnemonicTables.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86ShuffleDecode.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86WinCOFFObjectWriter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86WinCOFFStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/MCTargetDesc/X86WinCOFFTargetStreamer.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/TargetInfo/X86TargetInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86AsmPrinter.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86AvoidStoreForwardingBlocks.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86AvoidTrailingCall.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86CallFrameOptimization.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86CallingConv.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86CallLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86CmovConversion.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86DiscriminateMemOps.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86DomainReassignment.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86DynAllocaExpander.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86EvexToVex.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86ExpandPseudo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FastISel.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FastPreTileConfig.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FastTileConfig.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FixupBWInsts.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FixupLEAs.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FixupSetCC.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FlagsCopyLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FloatingPoint.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86FrameLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86IndirectBranchTracking.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86IndirectThunks.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86InsertPrefetch.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86InsertWait.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86InstCombineIntrinsic.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86InstrFMA3Info.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86InstrFoldTables.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86InstrInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86InstructionSelector.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86InterleavedAccess.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86ISelDAGToDAG.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86ISelLowering.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86KCFI.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86LegalizerInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86LoadValueInjectionLoadHardening.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86LoadValueInjectionRetHardening.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86LowerAMXIntrinsics.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86LowerAMXType.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86LowerTileCopy.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86MachineFunctionInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86MacroFusion.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86MCInstLower.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86OptimizeLEAs.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86PadShortFunction.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86PartialReduction.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86PreAMXConfig.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86PreTileConfig.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86RegisterBankInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86RegisterInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86ReturnThunks.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86SelectionDAGInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86ShuffleDecodeConstantPool.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86SpeculativeExecutionSideEffectSuppression.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86SpeculativeLoadHardening.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86Subtarget.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86TargetMachine.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86TargetObjectFile.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86TargetTransformInfo.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86TileConfig.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86VZeroUpper.cpp",
                "third_party/llvm-16.0/llvm/lib/Target/X86/X86WinEHState.cpp",
            },
            else => return error.UnsupportedCPUArchitecture,
        },
        .flags = llvm_cpp_flags.items,
    });

    return llvm;
}

fn build_llvm_reactor(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
    llvm_mode: JIT_Mode,
) !*std.Build.Step.Compile {
    const llvm = switch (llvm_mode) {
        .LLVMv10 => try build_llvm_v10(b, target, optimize, cpp_flags, default_flags, swiftshader_dep),
        .LLVMv16 => try build_llvm_v16(b, target, optimize, cpp_flags, default_flags, swiftshader_dep),
        else => unreachable,
    };

    const reactor_llvm_c_flags = blk: {
        var reactor_llvm_c_flags = std.ArrayList([]const u8).init(b.allocator);

        try reactor_llvm_c_flags.append("-std=c11");
        try reactor_llvm_c_flags.appendSlice(default_flags);
        try reactor_llvm_c_flags.append("-Werror");

        break :blk reactor_llvm_c_flags;
    };
    defer reactor_llvm_c_flags.deinit();

    const reactor_llvm_cpp_flags = blk: {
        var reactor_llvm_cpp_flags = std.ArrayList([]const u8).init(b.allocator);

        try reactor_llvm_cpp_flags.append("-std=c++17");
        try reactor_llvm_cpp_flags.appendSlice(default_flags);
        try reactor_llvm_cpp_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-deprecated-declarations",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wthread-safety",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try reactor_llvm_cpp_flags.appendSlice(cpp_flags);

        break :blk reactor_llvm_cpp_flags;
    };
    defer reactor_llvm_cpp_flags.deinit();

    const reactor_llvm = b.addStaticLibrary(.{
        .name = "ReactorLLVM",
        .target = target,
        .optimize = optimize,
    });
    reactor_llvm.linkLibrary(llvm);
    for (llvm.root_module.include_dirs.items) |dir| {
        reactor_llvm.addIncludePath(dir.path);
    }
    reactor_llvm.addIncludePath(swiftshader_dep.path("src/Reactor"));
    reactor_llvm.defineCMacro("__STDC_CONSTANT_MACROS", null);
    reactor_llvm.defineCMacro("__STDC_LIMIT_MACROS", null);
    reactor_llvm.defineCMacro("BLAKE3_NO_AVX2", null);
    reactor_llvm.defineCMacro("BLAKE3_NO_AVX512", null);
    reactor_llvm.defineCMacro("BLAKE3_NO_SSE2", null);
    reactor_llvm.defineCMacro("BLAKE3_NO_SSE41", null);
    reactor_llvm.defineCMacro("BLAKE3_USE_NEON", "0");
    reactor_llvm.defineCMacro("REACTOR_ANONYMOUS_MMAP_NAME", "swiftshader_jit");
    reactor_llvm.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    if (optimize != .Debug) reactor_llvm.defineCMacro("NDEBUG", null);
    if (target.result.os.tag == .windows) {
        reactor_llvm.defineCMacro("NOMINMAX", null);
        reactor_llvm.defineCMacro("STRICT", null);
        reactor_llvm.defineCMacro("WINVER", WINVER);
    }
    reactor_llvm.link_data_sections = true;
    reactor_llvm.link_function_sections = true;
    reactor_llvm.root_module.omit_frame_pointer = true;
    reactor_llvm.root_module.pic = true;

    const sources: []const []const u8 = &.{
        "src/Reactor/Assert.cpp",
        // "src/Reactor/CPUID.cpp",
        "src/Reactor/Debug.cpp",
        "src/Reactor/ExecutableMemory.cpp",
        "src/Reactor/LLVMAsm.cpp",
        "src/Reactor/LLVMJIT.cpp",
        "src/Reactor/LLVMReactor.cpp",
        "src/Reactor/LLVMReactorDebugInfo.cpp",
        "src/Reactor/Pragma.cpp",
        "src/Reactor/Reactor.cpp",
        "src/Reactor/ReactorDebugInfo.cpp",
        "src/Reactor/SIMD.cpp",
    };
    if (target.result.os.tag == .windows and (target.result.cpu.arch == .x86 or target.result.cpu.arch == .x86_64)) {
        const source_files = b.addWriteFiles();
        for (sources) |source| {
            reactor_llvm.addCSourceFile(.{
                .file = source_files.addCopyFile(swiftshader_dep.path(source), std.fs.path.basenamePosix(source)),
                .flags = reactor_llvm_cpp_flags.items,
            });
        }
        reactor_llvm.addCSourceFile(.{
            .file = source_files.addCopyFile(
                fix_windows_cpuidex(b, swiftshader_dep.path("src/Reactor/CPUID.cpp")),
                "CPUID.cpp",
            ),
            .flags = reactor_llvm_cpp_flags.items,
        });
    } else {
        for (sources) |source| {
            reactor_llvm.addCSourceFile(.{
                .file = swiftshader_dep.path(source),
                .flags = reactor_llvm_cpp_flags.items,
            });
        }
        reactor_llvm.addCSourceFile(.{
            .file = swiftshader_dep.path("src/Reactor/CPUID.cpp"),
            .flags = reactor_llvm_cpp_flags.items,
        });
    }

    if (target.result.os.tag == .windows) {
        switch (target.result.cpu.arch) {
            // For aarch64-window-*, zig exports a zig implementation of
            // __chkstk by default, so we don't stub it to avoid a duplicate
            // symbol error.
            .aarch64 => {},
            .x86, .x86_64 => if (target.result.abi.isGnu()) {
                // This does seem to have the potential to be used in LLVM, so
                // its not ideal to stub it like this. It seems to work without
                // crashing when stubbed at least, but this is something to
                // keep an eye on. Not sure why MinGW's _chkstk/__chkstk
                // implementations can't be found, it would be ideal to fix in
                // the future. For aarch64-window-*, zig exports a
                // zig implementation of __chkstk by default, so we don't stub
                // it to avoid a duplicate symbol error.
                reactor_llvm.addCSourceFile(.{
                    .file = b.path("src/stub_chkstk.c"),
                    .flags = reactor_llvm_c_flags.items,
                });
            },
            else => {},
        }
    }

    return reactor_llvm;
}

fn build_llvm_subzero(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
) !*std.Build.Step.Compile {
    const llvm_subzero_c_flags = blk: {
        var llvm_subzero_c_flags = std.ArrayList([]const u8).init(b.allocator);

        // llvm_subzero prefers to not set a -std= flag for these even though
        // they are C source files. This is not ideal, as it leaves the
        // potential for compiler updates to cause problems. Nevertheless, this
        // is the decision the upsteam project made, and this is merely a build
        // script, not an edict from on high.

        try llvm_subzero_c_flags.appendSlice(default_flags);
        try llvm_subzero_c_flags.appendSlice(&.{
            // "-fPIC",
            "-Werror",
            "-Wno-unused-parameter",
            "-Wno-deprecated-declarations",
        });

        break :blk llvm_subzero_c_flags;
    };
    defer llvm_subzero_c_flags.deinit();

    const llvm_subzero_cpp_flags = blk: {
        var llvm_subzero_cpp_flags = std.ArrayList([]const u8).init(b.allocator);

        try llvm_subzero_cpp_flags.append("-std=c++17");
        try llvm_subzero_cpp_flags.appendSlice(default_flags);
        try llvm_subzero_cpp_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Werror",
            "-Wno-deprecated-copy",
            "-Wno-unused-but-set-variable",
            "-Wno-unused-function",
            "-Wno-unused-parameter",
            "-Wno-unused-variable",
            "-Wno-deprecated-declarations",
        });
        try llvm_subzero_cpp_flags.appendSlice(cpp_flags);

        break :blk llvm_subzero_cpp_flags;
    };
    defer llvm_subzero_cpp_flags.deinit();

    const llvm_subzero = b.addStaticLibrary(.{
        .name = "llvm-subzero",
        .target = target,
        .optimize = optimize,
    });
    llvm_subzero.linkLibCpp();

    switch (target.result.os.tag) {
        .ios, .macos, .watchos, .tvos => llvm_subzero.addIncludePath(
            swiftshader_dep.path("third_party/llvm-subzero/build/MacOS/include")
        ),
        .linux => {
            if (target.result.abi.isMusl()) {
                const @"config.h" = fix_llvm_musl_config(
                    b,
                    swiftshader_dep.path("third_party/llvm-subzero/build/Linux/include/llvm/Config/config.h")
                );

                const wf = b.addWriteFiles();
                _ = wf.addCopyFile(@"config.h", "llvm/Config/config.h");
                llvm_subzero.addIncludePath(wf.getDirectory());
            }

            llvm_subzero.addIncludePath(swiftshader_dep.path("third_party/llvm-subzero/build/Linux/include"));
        },
        .windows => {
            llvm_subzero.defineCMacro("HAVE_LIBPSAPI", "1");
            llvm_subzero.defineCMacro("HAVE_LIBSHELL32", "1");
            // This is normally defined in
            // third_party/llvm-subzero/build/Windows/include/DataTypes.h, but
            // for some reason the definition found there isn't being pulled in
            // properly in some cases. So instead, we'll define it externally
            // to fix compilation. There are no conditional aspects to this
            // symbol on Windows, so it should be safe to do so.
            llvm_subzero.defineCMacro("PRId64", "\"I64d\"");
            // LLVM uses MSVC specific checks here to determine where to grab
            // the c++ ::write function from. For MinGW, it doesn't even
            // consider using <unistd.h>, so we must manually enable the flag
            // to use it.
            if (target.result.abi.isGnu()) {
                llvm_subzero.defineCMacro("HAVE_UNISTD_H", null);
            }
            llvm_subzero.addIncludePath(swiftshader_dep.path("third_party/llvm-subzero/build/Windows/include"));
        },
        else => return error.UnsupportedOS,
    }
    llvm_subzero.addIncludePath(swiftshader_dep.path("third_party/llvm-subzero/include"));
    if (optimize != .Debug) llvm_subzero.defineCMacro("NDEBUG", null);
    if (target.result.os.tag == .windows) {
        llvm_subzero.defineCMacro("NOMINMAX", null);
        llvm_subzero.defineCMacro("STRICT", null);
        llvm_subzero.defineCMacro("WINVER", WINVER);
    }
    llvm_subzero.link_data_sections = true; // "-fdata-sections",
    llvm_subzero.link_function_sections = true; // "-ffunction-sections",
    llvm_subzero.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    llvm_subzero.root_module.pic = true; // "-fPIC",
    llvm_subzero.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/llvm-subzero/lib/Demangle/ItaniumDemangle.cpp",
            "third_party/llvm-subzero/lib/Support/APInt.cpp",
            "third_party/llvm-subzero/lib/Support/Atomic.cpp",
            "third_party/llvm-subzero/lib/Support/circular_raw_ostream.cpp",
            "third_party/llvm-subzero/lib/Support/CommandLine.cpp",
            "third_party/llvm-subzero/lib/Support/Debug.cpp",
            "third_party/llvm-subzero/lib/Support/Errno.cpp",
            "third_party/llvm-subzero/lib/Support/ErrorHandling.cpp",
            "third_party/llvm-subzero/lib/Support/Hashing.cpp",
            "third_party/llvm-subzero/lib/Support/ManagedStatic.cpp",
            "third_party/llvm-subzero/lib/Support/MemoryBuffer.cpp",
            "third_party/llvm-subzero/lib/Support/Mutex.cpp",
            "third_party/llvm-subzero/lib/Support/NativeFormatting.cpp",
            "third_party/llvm-subzero/lib/Support/Path.cpp",
            "third_party/llvm-subzero/lib/Support/Process.cpp",
            "third_party/llvm-subzero/lib/Support/Program.cpp",
            "third_party/llvm-subzero/lib/Support/raw_os_ostream.cpp",
            "third_party/llvm-subzero/lib/Support/raw_ostream.cpp",
            "third_party/llvm-subzero/lib/Support/Regex.cpp",
            "third_party/llvm-subzero/lib/Support/Signals.cpp",
            "third_party/llvm-subzero/lib/Support/SmallPtrSet.cpp",
            "third_party/llvm-subzero/lib/Support/SmallVector.cpp",
            "third_party/llvm-subzero/lib/Support/StringExtras.cpp",
            "third_party/llvm-subzero/lib/Support/StringMap.cpp",
            "third_party/llvm-subzero/lib/Support/StringRef.cpp",
            "third_party/llvm-subzero/lib/Support/StringSaver.cpp",
            "third_party/llvm-subzero/lib/Support/Threading.cpp",
            "third_party/llvm-subzero/lib/Support/Timer.cpp",
            "third_party/llvm-subzero/lib/Support/Twine.cpp",
        },
        .flags = llvm_subzero_cpp_flags.items,
    });
    llvm_subzero.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/llvm-subzero/lib/Support/regcomp.c",
            "third_party/llvm-subzero/lib/Support/regerror.c",
            "third_party/llvm-subzero/lib/Support/regexec.c",
            "third_party/llvm-subzero/lib/Support/regfree.c",
            "third_party/llvm-subzero/lib/Support/regstrlcpy.c",
        },
        .flags = llvm_subzero_c_flags.items,
    });

    return llvm_subzero;
}

fn build_subzero(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
    llvm_subzero: *std.Build.Step.Compile,
) !*std.Build.Step.Compile {
    const subzero_c_flags = blk: {
        var subzero_c_flags = std.ArrayList([]const u8).init(b.allocator);

        try subzero_c_flags.append("-std=c11");
        try subzero_c_flags.appendSlice(default_flags);
        try subzero_c_flags.append("-Werror");

        break :blk subzero_c_flags;
    };
    defer subzero_c_flags.deinit();

    const subzero_cpp_flags = blk: {
        var subzero_cpp_flags = std.ArrayList([]const u8).init(b.allocator);

        try subzero_cpp_flags.append("-std=c++17");
        try subzero_cpp_flags.appendSlice(default_flags);
        try subzero_cpp_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-deprecated-declarations",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try subzero_cpp_flags.appendSlice(cpp_flags);

        if (target.result.cpu.arch == .x86) {
            try subzero_cpp_flags.append("-Wno-unused-variable");
        }

        break :blk subzero_cpp_flags;
    };
    defer subzero_cpp_flags.deinit();

    const subzero = b.addStaticLibrary(.{
        .name = "subzero",
        .target = target,
        .optimize = optimize,
    });
    subzero.linkLibrary(llvm_subzero);
    subzero.addIncludePath(swiftshader_dep.path("third_party/llvm-subzero/include"));
    subzero.addIncludePath(swiftshader_dep.path("third_party/subzero"));
    subzero.addIncludePath(swiftshader_dep.path("third_party/subzero/pnacl-llvm/include"));
    subzero.addIncludePath(swiftshader_dep.path(
        switch (target.result.os.tag) {
            .ios, .macos, .watchos, .tvos => "third_party/llvm-subzero/build/MacOS/include",
            .linux => "third_party/llvm-subzero/build/Linux/include",
            .windows => "third_party/llvm-subzero/build/Windows/include",
            else => return error.UnsupportedOS,
        }
    ));
    subzero.defineCMacro("ALLOW_DUMP", "0");
    subzero.defineCMacro("ALLOW_LLVM_CL", "0");
    subzero.defineCMacro("ALLOW_LLVM_IR_AS_INPUT", "0");
    subzero.defineCMacro("ALLOW_LLVM_IR", "0");
    subzero.defineCMacro("ALLOW_MINIMAL_BUILD", "0");
    subzero.defineCMacro("ALLOW_TIMERS", "0");
    subzero.defineCMacro("ALLOW_WASM", "0");
    subzero.defineCMacro("ICE_THREAD_LOCAL_HACK", "0");
    subzero.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    subzero.defineCMacro("SZTARGET", switch (target.result.cpu.arch) {
        .arm, => "ARM32",
        .mipsel => "MIPS32",
        .x86 => "X8632",
        .x86_64 => "X8664",
        else => "",
    });
    if (optimize != .Debug) subzero.defineCMacro("NDEBUG", null);
    if (target.result.os.tag == .windows) {
        subzero.defineCMacro("NOMINMAX", null);
        subzero.defineCMacro("STRICT", null);
        subzero.defineCMacro("WINVER", WINVER);
    }
    subzero.link_data_sections = true; // "-fdata-sections"
    subzero.link_function_sections = true; // "-ffunction-sections",
    subzero.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    subzero.root_module.pic = true; // "-fPIC",
    subzero.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/subzero/src/IceAssembler.cpp",
            "third_party/subzero/src/IceCfg.cpp",
            "third_party/subzero/src/IceCfgNode.cpp",
            "third_party/subzero/src/IceClFlags.cpp",
            "third_party/subzero/src/IceELFObjectWriter.cpp",
            "third_party/subzero/src/IceELFSection.cpp",
            "third_party/subzero/src/IceFixups.cpp",
            "third_party/subzero/src/IceGlobalContext.cpp",
            "third_party/subzero/src/IceGlobalInits.cpp",
            "third_party/subzero/src/IceInst.cpp",
            "third_party/subzero/src/IceInstrumentation.cpp",
            "third_party/subzero/src/IceIntrinsics.cpp",
            "third_party/subzero/src/IceLiveness.cpp",
            "third_party/subzero/src/IceLoopAnalyzer.cpp",
            "third_party/subzero/src/IceMangling.cpp",
            "third_party/subzero/src/IceMemory.cpp",
            "third_party/subzero/src/IceOperand.cpp",
            "third_party/subzero/src/IceRangeSpec.cpp",
            "third_party/subzero/src/IceRegAlloc.cpp",
            "third_party/subzero/src/IceRevision.cpp",
            "third_party/subzero/src/IceSwitchLowering.cpp",
            "third_party/subzero/src/IceTargetLowering.cpp",
            "third_party/subzero/src/IceThreading.cpp",
            "third_party/subzero/src/IceTimerTree.cpp",
            "third_party/subzero/src/IceTypes.cpp",
            "third_party/subzero/src/IceVariableSplitting.cpp",
        },
        .flags = subzero_cpp_flags.items,
    });
    subzero.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = switch (target.result.cpu.arch) {
            .arm => &.{
                "third_party/subzero/src/IceAssemblerARM32.cpp",
                "third_party/subzero/src/IceInstARM32.cpp",
                "third_party/subzero/src/IceTargetLoweringARM32.cpp",
            },
            .mipsel => &.{
                "third_party/subzero/src/IceAssemblerMIPS32.cpp",
                "third_party/subzero/src/IceInstMIPS32.cpp",
                "third_party/subzero/src/IceTargetLoweringMIPS32.cpp",
            },
            .x86 => &.{
                "third_party/subzero/src/IceAssemblerX8632.cpp",
                "third_party/subzero/src/IceInstX8632.cpp",
                "third_party/subzero/src/IceTargetLoweringX8632.cpp",
            },
            .x86_64 => &.{
                "third_party/subzero/src/IceAssemblerX8664.cpp",
                "third_party/subzero/src/IceInstX8664.cpp",
                "third_party/subzero/src/IceTargetLoweringX8664.cpp",
            },
            else => return error.UnsupportedCPUArchitecture,
        },
        .flags = subzero_cpp_flags.items,
    });
    if (target.result.os.tag == .windows and target.result.abi.isGnu() and target.result.cpu.arch != .aarch64) {
        // The check performed here to determine if __chkstk should be called
        // relies on MSVC details. On non-x86_64 MSVC targets, __chkstk isn't
        // used at all. This stubs out __chkstk which has the same effect for
        // x86_64 Windows. It might be nice to get this working for debugging
        // purposes, but not a priority. For aarch64-window-*, zig exports a
        // zig implementation of __chkstk by default, which would be a problem
        // and cause duplicate symbol errors, but subzero doesn't support
        // aarch64. Its only prevented here on the off chance at some point
        // Google writes an aarch64 backend for subzero.
        subzero.addCSourceFile(.{
            .file = b.path("src/stub_chkstk.c"),
            .flags = subzero_c_flags.items,
        });
    }

    return subzero;
}

fn build_subzero_reactor(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
    marl: *std.Build.Step.Compile,
) !*std.Build.Step.Compile {
    const llvm_subzero = try build_llvm_subzero(b, target, optimize, cpp_flags, default_flags, swiftshader_dep);
    const subzero = try build_subzero(b, target, optimize, cpp_flags, default_flags, swiftshader_dep, llvm_subzero);

    const reactor_subzero_flags = blk: {
        var reactor_subzero_flags = std.ArrayList([]const u8).init(b.allocator);

        try reactor_subzero_flags.append("-std=c++17");
        try reactor_subzero_flags.appendSlice(default_flags);
        try reactor_subzero_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-deprecated-declarations",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wthread-safety",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try reactor_subzero_flags.appendSlice(cpp_flags);

        break :blk reactor_subzero_flags;
    };
    defer reactor_subzero_flags.deinit();

    const reactor_subzero = b.addStaticLibrary(.{
        .name = "ReactorSubzero",
        .target = target,
        .optimize = optimize,
    });
    reactor_subzero.linkLibrary(llvm_subzero);
    reactor_subzero.linkLibrary(marl);
    reactor_subzero.linkLibrary(subzero);
    reactor_subzero.addIncludePath(swiftshader_dep.path("src/Reactor"));
    reactor_subzero.addIncludePath(swiftshader_dep.path("third_party/llvm-subzero/include"));
    reactor_subzero.addIncludePath(swiftshader_dep.path("third_party/marl/include"));
    reactor_subzero.addIncludePath(swiftshader_dep.path("third_party/subzero"));
    reactor_subzero.addIncludePath(swiftshader_dep.path("third_party/subzero/pnacl-llvm/include"));
    switch (target.result.os.tag) {
        .ios, .macos, .watchos, .tvos => reactor_subzero.addIncludePath(
            swiftshader_dep.path("third_party/llvm-subzero/build/MacOS/include")
        ),
        .linux => {
            reactor_subzero.addIncludePath(swiftshader_dep.path("third_party/llvm-subzero/build/Linux/include"));
            reactor_subzero.addIncludePath(swiftshader_dep.path("include/Wayland"));
        },
        .windows => {
            reactor_subzero.addIncludePath(swiftshader_dep.path("third_party/llvm-subzero/build/Windows/include"));
        },
        else => return error.UnsupportedOS,
    }
    reactor_subzero.defineCMacro("ALLOW_DUMP", "0");
    reactor_subzero.defineCMacro("ALLOW_LLVM_CL", "0");
    reactor_subzero.defineCMacro("ALLOW_LLVM_IR_AS_INPUT", "0");
    reactor_subzero.defineCMacro("ALLOW_LLVM_IR", "0");
    reactor_subzero.defineCMacro("ALLOW_MINIMAL_BUILD", "0");
    reactor_subzero.defineCMacro("ALLOW_TIMERS", "0");
    reactor_subzero.defineCMacro("ALLOW_WASM", "0");
    reactor_subzero.defineCMacro("ICE_THREAD_LOCAL_HACK", "0");
    reactor_subzero.defineCMacro("REACTOR_ANONYMOUS_MMAP_NAME", "swiftshader_jit");
    reactor_subzero.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    reactor_subzero.defineCMacro("SZTARGET", switch (target.result.cpu.arch) {
        .arm, => "ARM32",
        .mipsel => "MIPS32",
        .x86 => "X8632",
        .x86_64 => "X8664",
        else => "",
    });
    if (optimize != .Debug) reactor_subzero.defineCMacro("NDEBUG", null);
    if (target.result.os.tag == .windows) {
        reactor_subzero.defineCMacro("NOMINMAX", null);
        reactor_subzero.defineCMacro("STRICT", null);
        reactor_subzero.defineCMacro("WINVER", WINVER);
    }
    reactor_subzero.link_data_sections = true; // "-fdata-sections",
    reactor_subzero.link_function_sections = true; // "-ffunction-sections",
    reactor_subzero.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    reactor_subzero.root_module.pic = true; // "-fPIC",
    reactor_subzero.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "src/Reactor/Assert.cpp",
            "src/Reactor/Debug.cpp",
            "src/Reactor/ExecutableMemory.cpp",
            "src/Reactor/Optimizer.cpp",
            "src/Reactor/Pragma.cpp",
            "src/Reactor/Reactor.cpp",
            "src/Reactor/ReactorDebugInfo.cpp",
            "src/Reactor/SIMD.cpp",
        },
        .flags = reactor_subzero_flags.items,
    });

    const @"SubzeroReactor.cpp" = switch (target.result.os.tag == .windows and target.result.abi.isGnu()) {
        true => blk: {
            // For MSVC, __cpuid() is a compiler intrinsic. It is also available as
            // a macro provided by MinGW, if one includes the intrin.h header.
            // SubzeroReactor.cpp makes no attempts to include the header if
            // compiled for MinGW, so it must be added manually.
            const fix_llvm_windows_gnu_subzero_reactor = b.addExecutable(.{
                .name = "fix_llvm_windows_gnu_subzero_reactor",
                .root_source_file = b.path("src/fix_windows_gnu_subzero_reactor.zig"),
                .target = b.host,
                .optimize = .Debug
            });
            const run_fix_llvm_windows_gnu_subzero_reactor = b.addRunArtifact(fix_llvm_windows_gnu_subzero_reactor);
            run_fix_llvm_windows_gnu_subzero_reactor.addFileArg(
                swiftshader_dep.path("src/Reactor/SubzeroReactor.cpp")
            );
            break :blk run_fix_llvm_windows_gnu_subzero_reactor.addOutputFileArg("SubzeroReactor.cpp");
        },
        else => swiftshader_dep.path("src/Reactor/SubzeroReactor.cpp"),
    };
    reactor_subzero.addCSourceFile(.{
        .file = @"SubzeroReactor.cpp",
        .flags = reactor_subzero_flags.items,
    });

    return reactor_subzero;
}

fn build_astc_encoder(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
) !*std.Build.Step.Compile {
    const astc_encoder_flags = blk: {
        var astc_encoder_flags = std.ArrayList([]const u8).init(b.allocator);

        try astc_encoder_flags.append("-std=c++17");
        try astc_encoder_flags.appendSlice(default_flags);
        try astc_encoder_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wthread-safety",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try astc_encoder_flags.appendSlice(cpp_flags);

        break :blk astc_encoder_flags;
    };
    defer astc_encoder_flags.deinit();

    const astc_encoder = b.addStaticLibrary(.{
        .name = "astc-encoder",
        .target = target,
        .optimize = optimize,
    });
    astc_encoder.linkLibCpp();
    astc_encoder.addIncludePath(swiftshader_dep.path("third_party/astc-encoder/Source"));
    astc_encoder.defineCMacro("SWIFTSHADER_ENABLE_ASTC", null);
    astc_encoder.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    if (optimize != .Debug) astc_encoder.defineCMacro("NDEBUG", null);
    if (target.result.os.tag == .windows) {
        astc_encoder.defineCMacro("NOMINMAX", null);
        astc_encoder.defineCMacro("STRICT", null);
        astc_encoder.defineCMacro("WINVER", WINVER);
    }
    astc_encoder.link_data_sections = true; // "-fdata-sections",
    astc_encoder.link_function_sections = true; // "-ffunction-sections",
    astc_encoder.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    astc_encoder.root_module.pic = true; // "-fPIC",
    astc_encoder.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "third_party/astc-encoder/Source/astc_block_sizes2.cpp",
            "third_party/astc-encoder/Source/astc_color_unquantize.cpp",
            "third_party/astc-encoder/Source/astc_decompress_symbolic.cpp",
            "third_party/astc-encoder/Source/astc_image_load_store.cpp",
            "third_party/astc-encoder/Source/astc_integer_sequence.cpp",
            "third_party/astc-encoder/Source/astc_mathlib_softfloat.cpp",
            "third_party/astc-encoder/Source/astc_mathlib.cpp",
            "third_party/astc-encoder/Source/astc_partition_tables.cpp",
            "third_party/astc-encoder/Source/astc_quantization.cpp",
            "third_party/astc-encoder/Source/astc_symbolic_physical.cpp",
            "third_party/astc-encoder/Source/astc_weight_quant_xfer_tables.cpp",
        },
        .flags = astc_encoder_flags.items,
    });

    return astc_encoder;
}

fn build_vk_system(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
    marl: *std.Build.Step.Compile,
) !*std.Build.Step.Compile {
    const vk_system_flags = blk: {
        var vk_system_flags = std.ArrayList([]const u8).init(b.allocator);

        try vk_system_flags.append("-std=c++17");
        try vk_system_flags.appendSlice(default_flags);
        try vk_system_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            // Copied from SwiftShader CMakeLists.txt, disable this warning for
            // Clang until they fix it upstream.
            // # TODO(b/208256248): Avoid exit-time destructor.
            // #"-Wexit-time-destructors",  # declaration requires an exit-time destructor
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wthread-safety",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try vk_system_flags.appendSlice(cpp_flags);

        break :blk vk_system_flags;
    };
    defer vk_system_flags.deinit();

    const vk_system = b.addStaticLibrary(.{
        .name = "vk_system",
        .target = target,
        .optimize = optimize,
    });
    vk_system.linkLibrary(marl);
    vk_system.addIncludePath(swiftshader_dep.path("src"));
    vk_system.addIncludePath(swiftshader_dep.path("third_party/marl/include"));
    if (target.result.os.tag == .linux) {
        vk_system.addIncludePath(swiftshader_dep.path("include/Wayland"));
    }
    vk_system.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    if (optimize != .Debug) vk_system.defineCMacro("NDEBUG", null);
    if (target.result.os.tag == .windows) {
        vk_system.defineCMacro("NOMINMAX", null);
        vk_system.defineCMacro("STRICT", null);
        vk_system.defineCMacro("WINVER", WINVER);

        // _ReadStatusReg is a compiler intrinsic provided by MSVC. MinGW
        // defines a header for it, but does not define an implementation. It
        // would be nice to provide a correct implementation here, but as far
        // as I can see, there is no way at the C/C++ level to call the LLVM
        // intrinsic implementation which can read the appropriate register.
        // I could maybe implement it in zig with either inline assembly or
        // by using the LLVM intrinsic directly, but working with assembly is
        // beyond my ken.
        if (target.result.cpu.arch == .aarch64) {
            // The default behavior for other architectures besides
            // x86/x86_64/aarch64 is just to return 0 here, this define will
            // change aarch64 to do the same.
            vk_system.defineCMacro("_ReadStatusReg(ARM64_PMCCNTR_EL0)", "0");
        }
    }
    vk_system.link_data_sections = true; // "-fdata-sections",
    vk_system.link_function_sections = true; // "-ffunction-sections",
    vk_system.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    vk_system.root_module.pic = true; // "-fPIC",
    vk_system.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "src/System/Build.cpp",
            "src/System/Configurator.cpp",
            "src/System/CPUID.cpp",
            "src/System/Debug.cpp",
            "src/System/Half.cpp",
            "src/System/Math.cpp",
            "src/System/Memory.cpp",
            "src/System/Socket.cpp",
            "src/System/SwiftConfig.cpp",
            "src/System/Timer.cpp",
        },
        .flags = vk_system_flags.items,
    });
    if (target.result.os.tag == .linux) {
        vk_system.addCSourceFile(.{
            .file = swiftshader_dep.path("src/System/Linux/MemFd.cpp"),
            .flags = vk_system_flags.items,
        });
    }

    return vk_system;
}

fn build_vk_pipeline(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
    marl: *std.Build.Step.Compile,
    reactor: *std.Build.Step.Compile,
    spirv_tools: *std.Build.Step.Compile,
    vk_system: *std.Build.Step.Compile,
) !*std.Build.Step.Compile {
    const vk_pipeline_flags = blk: {
        var vk_pipeline_flags = std.ArrayList([]const u8).init(b.allocator);

        try vk_pipeline_flags.append("-std=c++17");
        try vk_pipeline_flags.appendSlice(default_flags);
        try vk_pipeline_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            "-Wexit-time-destructors",
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wthread-safety",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try vk_pipeline_flags.appendSlice(cpp_flags);

        break :blk vk_pipeline_flags;
    };
    defer vk_pipeline_flags.deinit();

    const vk_pipeline = b.addStaticLibrary(.{
        .name = "vk_pipeline",
        .target = target,
        .optimize = optimize,
    });
    vk_pipeline.linkLibrary(marl);
    vk_pipeline.linkLibrary(reactor);
    vk_pipeline.linkLibrary(spirv_tools);
    vk_pipeline.linkLibrary(vk_system);
    vk_pipeline.addIncludePath(swiftshader_dep.path("include"));
    vk_pipeline.addIncludePath(swiftshader_dep.path("src"));
    vk_pipeline.addIncludePath(swiftshader_dep.path("src/Pipeline"));
    vk_pipeline.addIncludePath(swiftshader_dep.path("src/Reactor"));
    vk_pipeline.addIncludePath(swiftshader_dep.path("third_party/SPIRV-Headers/include"));
    vk_pipeline.addIncludePath(swiftshader_dep.path("third_party/SPIRV-Tools/include"));
    vk_pipeline.addIncludePath(swiftshader_dep.path("third_party/marl/include"));
    vk_pipeline.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    if (optimize != .Debug) vk_pipeline.defineCMacro("NDEBUG", null);
    switch (target.result.os.tag) {
        .ios, .macos, .watchos, .tvos => {
            vk_pipeline.defineCMacro("VK_USE_PLATFORM_MACOS_MVK", null);
            vk_pipeline.defineCMacro("VK_USE_PLATFORM_METAL_EXT", null);
        },
        .linux => {
            vk_pipeline.defineCMacro("VK_USE_PLATFORM_WAYLAND_KHR", null);
            vk_pipeline.defineCMacro("VK_USE_PLATFORM_XCB_KHR", null);
        },
        .windows => {
            vk_pipeline.defineCMacro("NOMINMAX", null);
            vk_pipeline.defineCMacro("STRICT", null);
            vk_pipeline.defineCMacro("VK_USE_PLATFORM_WIN32_KHR", null);
            vk_pipeline.defineCMacro("WINVER", WINVER);
        },
        else => {},
    }
    vk_pipeline.link_data_sections = true; // "-fdata-sections",
    vk_pipeline.link_function_sections = true; // "-ffunction-sections",
    vk_pipeline.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    vk_pipeline.root_module.pic = true; // "-fPIC",
    vk_pipeline.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "src/Pipeline/ComputeProgram.cpp",
            "src/Pipeline/Constants.cpp",
            "src/Pipeline/PixelProgram.cpp",
            "src/Pipeline/PixelRoutine.cpp",
            "src/Pipeline/SamplerCore.cpp",
            "src/Pipeline/SetupRoutine.cpp",
            "src/Pipeline/ShaderCore.cpp",
            "src/Pipeline/SpirvBinary.cpp",
            "src/Pipeline/SpirvProfiler.cpp",
            "src/Pipeline/SpirvShader.cpp",
            "src/Pipeline/SpirvShaderArithmetic.cpp",
            "src/Pipeline/SpirvShaderControlFlow.cpp",
            "src/Pipeline/SpirvShaderDebugger.cpp",
            "src/Pipeline/SpirvShaderGLSLstd450.cpp",
            "src/Pipeline/SpirvShaderGroup.cpp",
            "src/Pipeline/SpirvShaderImage.cpp",
            "src/Pipeline/SpirvShaderInstructions.cpp",
            "src/Pipeline/SpirvShaderMemory.cpp",
            "src/Pipeline/SpirvShaderSampling.cpp",
            "src/Pipeline/SpirvShaderSpec.cpp",
            "src/Pipeline/VertexProgram.cpp",
            "src/Pipeline/VertexRoutine.cpp",
        },
        .flags = vk_pipeline_flags.items,
    });

    return vk_pipeline;
}

fn build_vk_wsi(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
    xcode_lazy_dep: ?*std.Build.Dependency,
    marl: *std.Build.Step.Compile,
    reactor: *std.Build.Step.Compile,
    spirv_tools: *std.Build.Step.Compile,
    vk_pipeline: *std.Build.Step.Compile,
    vk_system: *std.Build.Step.Compile,
) !*std.Build.Step.Compile {
    const vk_wsi_flags = blk: {
        var vk_wsi_flags = std.ArrayList([]const u8).init(b.allocator);

        try vk_wsi_flags.append("-std=c++17");
        try vk_wsi_flags.appendSlice(default_flags);
        try vk_wsi_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            "-Wexit-time-destructors",
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wthread-safety",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try vk_wsi_flags.appendSlice(cpp_flags);

        if (target.result.os.tag == .windows) {
            try vk_wsi_flags.append("-Wno-unused-variable");
        }

        break :blk vk_wsi_flags;
    };
    defer vk_wsi_flags.deinit();

    const vk_wsi = b.addStaticLibrary(.{
        .name = "vk_wsi",
        .target = target,
        .optimize = optimize,
    });
    if (target.result.os.tag.isDarwin()) {
        include_macos_libraries(vk_wsi, xcode_lazy_dep);
    }
    vk_wsi.linkLibrary(marl);
    vk_wsi.linkLibrary(reactor);
    vk_wsi.linkLibrary(spirv_tools);
    vk_wsi.linkLibrary(vk_pipeline);
    vk_wsi.linkLibrary(vk_system);
    vk_wsi.addIncludePath(swiftshader_dep.path("include"));
    vk_wsi.addIncludePath(swiftshader_dep.path("src"));
    vk_wsi.addIncludePath(swiftshader_dep.path("src/Pipeline"));
    vk_wsi.addIncludePath(swiftshader_dep.path("src/Reactor"));
    vk_wsi.addIncludePath(swiftshader_dep.path("third_party/marl/include"));
    vk_wsi.addIncludePath(swiftshader_dep.path("third_party/SPIRV-Headers/include"));
    vk_wsi.addIncludePath(swiftshader_dep.path("third_party/SPIRV-Tools/include"));
    vk_wsi.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    if (optimize != .Debug) vk_wsi.defineCMacro("NDEBUG", null);
    switch (target.result.os.tag) {
        .ios, .macos, .watchos, .tvos => {
            vk_wsi.defineCMacro("VK_USE_PLATFORM_MACOS_MVK", null);
            vk_wsi.defineCMacro("VK_USE_PLATFORM_METAL_EXT", null);
        },
        .linux => {
            vk_wsi.addIncludePath(swiftshader_dep.path("include/Wayland"));
            vk_wsi.defineCMacro("VK_USE_PLATFORM_WAYLAND_KHR", null);
            vk_wsi.defineCMacro("VK_USE_PLATFORM_XCB_KHR", null);
        },
        .windows => {
            vk_wsi.defineCMacro("NOMINMAX", null);
            vk_wsi.defineCMacro("STRICT", null);
            vk_wsi.defineCMacro("VK_USE_PLATFORM_WIN32_KHR", null);
            vk_wsi.defineCMacro("WINVER", WINVER);
        },
        else => {},
    }
    vk_wsi.link_data_sections = true; // "-fdata-sections",
    vk_wsi.link_function_sections = true; // "-ffunction-sections",
    vk_wsi.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    vk_wsi.root_module.pic = true; // "-fPIC",
    vk_wsi.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "src/WSI/HeadlessSurfaceKHR.cpp",
            "src/WSI/VkSurfaceKHR.cpp",
            "src/WSI/VkSwapchainKHR.cpp",
        },
        .flags = vk_wsi_flags.items,
    });
    vk_wsi.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = switch (target.result.os.tag) {
            .ios, .macos, .watchos, .tvos => &.{
                "src/WSI/MetalSurface.mm",
            },
            .linux => &.{
                "src/WSI/libWaylandClient.cpp",
                "src/WSI/libXCB.cpp",
                "src/WSI/WaylandSurfaceKHR.cpp",
                "src/WSI/XcbSurfaceKHR.cpp",
            },
            .windows => &.{
                "src/WSI/Win32SurfaceKHR.cpp",
            },
            else => return error.UnsupportedOS,
        },
        .flags = vk_wsi_flags.items,
    });

    return vk_wsi;
}

fn build_vk_device(
    b: *std.Build,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
    astc_encoder: *std.Build.Step.Compile,
    marl: *std.Build.Step.Compile,
    reactor: *std.Build.Step.Compile,
    spirv_tools: *std.Build.Step.Compile,
    vk_pipeline: *std.Build.Step.Compile,
    vk_system: *std.Build.Step.Compile,
) !*std.Build.Step.Compile {
    const vk_device_flags = blk: {
        var vk_device_flags = std.ArrayList([]const u8).init(b.allocator);

        try vk_device_flags.append("-std=c++17");
        try vk_device_flags.appendSlice(default_flags);
        try vk_device_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wthread-safety",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try vk_device_flags.appendSlice(cpp_flags);

        break :blk vk_device_flags;
    };
    defer vk_device_flags.deinit();

    const vk_device = b.addStaticLibrary(.{
        .name = "vk_device",
        .target = target,
        .optimize = optimize,
    });
    vk_device.linkLibrary(astc_encoder);
    vk_device.linkLibrary(marl);
    vk_device.linkLibrary(reactor);
    vk_device.linkLibrary(spirv_tools);
    vk_device.linkLibrary(vk_pipeline);
    vk_device.linkLibrary(vk_system);
    vk_device.addIncludePath(swiftshader_dep.path("include"));
    vk_device.addIncludePath(swiftshader_dep.path("src"));
    vk_device.addIncludePath(swiftshader_dep.path("src/Pipeline"));
    vk_device.addIncludePath(swiftshader_dep.path("src/Reactor"));
    vk_device.addIncludePath(swiftshader_dep.path("third_party/astc-encoder/Source"));
    vk_device.addIncludePath(swiftshader_dep.path("third_party/marl/include"));
    vk_device.addIncludePath(swiftshader_dep.path("third_party/SPIRV-Headers/include"));
    vk_device.addIncludePath(swiftshader_dep.path("third_party/SPIRV-Tools/include"));
    vk_device.defineCMacro("SWIFTSHADER_ENABLE_ASTC", null);
    vk_device.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    if (optimize != .Debug) vk_device.defineCMacro("NDEBUG", null);
    switch (target.result.os.tag) {
        .ios, .macos, .watchos, .tvos => {
            vk_device.defineCMacro("VK_USE_PLATFORM_MACOS_MVK", null);
            vk_device.defineCMacro("VK_USE_PLATFORM_METAL_EXT", null);
        },
        .linux => {
            vk_device.addIncludePath(swiftshader_dep.path("include/Wayland"));
            vk_device.defineCMacro("VK_USE_PLATFORM_WAYLAND_KHR", null);
            vk_device.defineCMacro("VK_USE_PLATFORM_XCB_KHR", null);
        },
        .windows => {
            vk_device.defineCMacro("NOMINMAX", null);
            vk_device.defineCMacro("STRICT", null);
            vk_device.defineCMacro("VK_USE_PLATFORM_WIN32_KHR", null);
            vk_device.defineCMacro("WINVER", WINVER);
        },
        else => {},
    }
    vk_device.link_data_sections = true; // "-fdata-sections",
    vk_device.link_function_sections = true; // "-ffunction-sections",
    vk_device.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    vk_device.root_module.pic = true; // "-fPIC",
    vk_device.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "src/Device/ASTC_Decoder.cpp",
            "src/Device/BC_Decoder.cpp",
            "src/Device/Blitter.cpp",
            "src/Device/Clipper.cpp",
            "src/Device/Context.cpp",
            "src/Device/ETC_Decoder.cpp",
            "src/Device/PixelProcessor.cpp",
            "src/Device/QuadRasterizer.cpp",
            "src/Device/Renderer.cpp",
            "src/Device/SetupProcessor.cpp",
            "src/Device/VertexProcessor.cpp",
        },
        .flags = vk_device_flags.items,
    });

    return vk_device;
}

fn build_vk_swiftshader(
    b: *std.Build,
    name: []const u8,
    linkage: std.builtin.LinkMode,
    target: std.Build.ResolvedTarget,
    optimize: std.builtin.OptimizeMode,
    cpp_flags: []const []const u8,
    default_flags: []const []const u8,
    swiftshader_dep: *std.Build.Dependency,
    xcode_lazy_dep: ?*std.Build.Dependency,
    astc_encoder: *std.Build.Step.Compile,
    marl: *std.Build.Step.Compile,
    reactor: *std.Build.Step.Compile,
    spirv_tools: *std.Build.Step.Compile,
    vk_device: *std.Build.Step.Compile,
    vk_pipeline: *std.Build.Step.Compile,
    vk_system: *std.Build.Step.Compile,
    vk_wsi: *std.Build.Step.Compile,
) !*std.Build.Step.Compile {
    const vk_swiftshader_flags = blk: {
        var vk_swiftshader_flags = std.ArrayList([]const u8).init(b.allocator);

        try vk_swiftshader_flags.append("-std=c++17");
        try vk_swiftshader_flags.appendSlice(default_flags);
        try vk_swiftshader_flags.appendSlice(&.{
            // "-fdata-sections",
            // "-ffunction-sections",
            // "-fomit-frame-pointer",
            // "-fPIC",
            "-Wdeprecated-copy",
            "-Werror",
            "-Wextra-semi",
            "-Wignored-qualifiers",
            "-Wmissing-braces",
            "-Wno-comment",
            "-Wno-dll-attribute-on-redeclaration",
            "-Wno-extra-semi",
            "-Wno-unknown-warning-option",
            "-Wno-unneeded-internal-declaration",
            "-Wno-unused-parameter",
            "-Wno-unused-private-field",
            "-Wreorder",
            "-Wsign-compare",
            "-Wstring-conversion",
            "-Wthread-safety",
            "-Wunreachable-code-loop-increment",
            "-Wunused-lambda-capture",
        });
        try vk_swiftshader_flags.appendSlice(cpp_flags);

        break :blk vk_swiftshader_flags;
    };
    defer vk_swiftshader_flags.deinit();

    const vk_swiftshader = switch (linkage) {
        .dynamic => b.addSharedLibrary(.{
            .name = name,
            .target = target,
            .optimize = optimize,
        }),
        .static => b.addStaticLibrary(.{
            .name = name,
            .target = target,
            .optimize = optimize,
        }),
    };

    switch (target.result.os.tag) {
        .ios, .macos, .watchos, .tvos => include_macos_libraries(vk_swiftshader, xcode_lazy_dep),
        .linux => switch (target.result.cpu.arch) {
            // https://github.com/ziglang/zig/issues/7935#issuecomment-1258071152
            .x86 => vk_swiftshader.link_z_notext = target.result.abi.isMusl(),
            .mipsel, .mips64el => vk_swiftshader.link_z_notext = true,
            else => {},
        },
        else => {},
    }

    vk_swiftshader.linkLibrary(astc_encoder);
    vk_swiftshader.linkLibrary(marl);
    vk_swiftshader.linkLibrary(reactor);
    vk_swiftshader.linkLibrary(spirv_tools);
    vk_swiftshader.linkLibrary(vk_device);
    vk_swiftshader.linkLibrary(vk_pipeline);
    vk_swiftshader.linkLibrary(vk_system);
    vk_swiftshader.linkLibrary(vk_wsi);
    if (target.result.os.tag == .windows) {
        vk_swiftshader.linkSystemLibrary("gdi32");
        vk_swiftshader.linkSystemLibrary("ole32");
    }
    vk_swiftshader.addIncludePath(swiftshader_dep.path("include"));
    vk_swiftshader.addIncludePath(swiftshader_dep.path("src"));
    vk_swiftshader.addIncludePath(swiftshader_dep.path("src/Pipeline"));
    vk_swiftshader.addIncludePath(swiftshader_dep.path("src/Reactor"));
    vk_swiftshader.addIncludePath(swiftshader_dep.path("third_party/astc-encoder/Source"));
    vk_swiftshader.addIncludePath(swiftshader_dep.path("third_party/marl/include"));
    vk_swiftshader.addIncludePath(swiftshader_dep.path("third_party/SPIRV-Headers/include"));
    vk_swiftshader.addIncludePath(swiftshader_dep.path("third_party/SPIRV-Tools/include"));
    vk_swiftshader.defineCMacro("SWIFTSHADER_ENABLE_ASTC", null);
    vk_swiftshader.defineCMacro("SWIFTSHADER_LOGGING_LEVEL", "Info");
    vk_swiftshader.defineCMacro("vk_swiftshader_EXPORTS", null);
    if (optimize != .Debug) vk_swiftshader.defineCMacro("NDEBUG", null);
    switch (target.result.os.tag) {
        .ios, .macos, .watchos, .tvos => {
            vk_swiftshader.defineCMacro("VK_EXPORT", "__attribute__((visibility(\"default\")))");
            vk_swiftshader.defineCMacro("VK_USE_PLATFORM_MACOS_MVK", null);
            vk_swiftshader.defineCMacro("VK_USE_PLATFORM_METAL_EXT", null);
        },
        .linux => {
            vk_swiftshader.addIncludePath(swiftshader_dep.path("include/Wayland"));
            vk_swiftshader.defineCMacro("VK_EXPORT", "__attribute__((visibility(\"default\")))");
            vk_swiftshader.defineCMacro("VK_USE_PLATFORM_WAYLAND_KHR", null);
            vk_swiftshader.defineCMacro("VK_USE_PLATFORM_XCB_KHR", null);

            if (linkage == .dynamic) {
                vk_swiftshader.linker_allow_undefined_version = true;
                vk_swiftshader.setVersionScript(swiftshader_dep.path("src/Vulkan/vk_swiftshader.lds"));
            }
        },
        .windows => {
            vk_swiftshader.defineCMacro("NOMINMAX", null);
            vk_swiftshader.defineCMacro("STRICT", null);
            vk_swiftshader.defineCMacro("VK_USE_PLATFORM_WIN32_KHR", null);
            vk_swiftshader.defineCMacro("WINVER", WINVER);

            // Is this more portable than `__declspec(dllexport)`? Both seem to
            // work fine with zig's C compiler.
            // https://learn.microsoft.com/en-us/cpp/cpp/dllexport-dllimport
            // calls `__declspec(dllexport)` "Microsoft-specific extensions to
            // the C and C++ languages", but I cannot find anything describing
            // the C/C++ standard explicitly supporting
            // `__attribute__((dllexport))`, so are they both non-standard
            // extensions that zig's C compiler happens to support?
            vk_swiftshader.defineCMacro("VK_EXPORT", "__attribute__((dllexport))");
        },
        else => vk_swiftshader.defineCMacro("VK_EXPORT", "__attribute__((visibility(\"default\")))"),
    }
    vk_swiftshader.link_data_sections = true; // "-fdata-sections",
    vk_swiftshader.link_function_sections = true; // "-ffunction-sections",
    vk_swiftshader.root_module.omit_frame_pointer = true; // "-fomit-frame-pointer",
    vk_swiftshader.root_module.pic = true; // "-fPIC",
    vk_swiftshader.addCSourceFiles(.{
        .root = swiftshader_dep.path(""),
        .files = &.{
            "src/Vulkan/libVulkan.cpp",
            "src/Vulkan/main.cpp",
            "src/Vulkan/VkBuffer.cpp",
            "src/Vulkan/VkBufferView.cpp",
            "src/Vulkan/VkCommandBuffer.cpp",
            "src/Vulkan/VkCommandPool.cpp",
            "src/Vulkan/VkDebugUtilsMessenger.cpp",
            "src/Vulkan/VkDescriptorPool.cpp",
            "src/Vulkan/VkDescriptorSet.cpp",
            "src/Vulkan/VkDescriptorSetLayout.cpp",
            "src/Vulkan/VkDescriptorUpdateTemplate.cpp",
            "src/Vulkan/VkDevice.cpp",
            "src/Vulkan/VkDeviceMemory.cpp",
            "src/Vulkan/VkDeviceMemoryExternalHost.cpp",
            "src/Vulkan/VkFormat.cpp",
            "src/Vulkan/VkFramebuffer.cpp",
            "src/Vulkan/VkGetProcAddress.cpp",
            "src/Vulkan/VkImage.cpp",
            "src/Vulkan/VkImageView.cpp",
            "src/Vulkan/VkInstance.cpp",
            "src/Vulkan/VkMemory.cpp",
            "src/Vulkan/VkPhysicalDevice.cpp",
            "src/Vulkan/VkPipeline.cpp",
            "src/Vulkan/VkPipelineCache.cpp",
            "src/Vulkan/VkPipelineLayout.cpp",
            "src/Vulkan/VkPromotedExtensions.cpp",
            "src/Vulkan/VkQueryPool.cpp",
            "src/Vulkan/VkQueue.cpp",
            "src/Vulkan/VkRenderPass.cpp",
            "src/Vulkan/VkSampler.cpp",
            "src/Vulkan/VkSemaphore.cpp",
            "src/Vulkan/VkShaderModule.cpp",
            "src/Vulkan/VkSpecializationInfo.cpp",
            "src/Vulkan/VkStringify.cpp",
            "src/Vulkan/VkTimelineSemaphore.cpp",
        },
        .flags = vk_swiftshader_flags.items,
    });

    return vk_swiftshader;
}

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = blk: {
        const target = b.standardTargetOptions(.{});

        switch (target.result.cpu.arch) {
            .mips64el => {
                const baseline = std.Target.Cpu.baseline(.mips64el);

                const is_baseline_model = std.mem.eql(u8, target.result.cpu.model.name, baseline.model.name);
                const no_extra_features = target.result.cpu.features.eql(baseline.features);
                // if something has intentionally been set, do not override
                if (!(is_baseline_model and no_extra_features)) break :blk target;
            },
            else => break :blk target,
        }

        const triple = try target.result.zigTriple(b.allocator);
        defer b.allocator.free(triple);
        // This should honestly be the default for this architecture in zig.
        const new_query = try std.Build.parseTargetQuery(.{ .arch_os_abi = triple, .cpu_features = "baseline+xgot" });
        break :blk b.resolveTargetQuery(new_query);
    };

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const default_symbol_visibility_raw = b.option(
        []const u8,
        "default_symbol_visibility",
        "Set the value that is passed to the -fvisibility flag. " ++
        "Default=hidden",
    )
        orelse "hidden";

    const disable_cpp_exceptions =
        if (b.option(
            bool,
            "disable_cpp_exceptions",
            "Should -fno-exceptions option be given when compiling C++ " ++
            "source files. Default=true",
        )) |opt|
            opt
        else
            true;

    // This issue says its only a problem on MacOS, but actually its a problem
    // on all platforms. This option won't work as intended until this issue is
    // fixed.
    // https://github.com/ziglang/zig/issues/11403
    const enable_asan =
        if (b.option(
            bool,
            "enable_asan",
            "Enable the Address Sanitizer. This option cannot be enabled " ++
            "at the same time as the Thread Sanitizer. Default=false",
        )) |opt|
            opt
        else
            false;

    // This issue applies for the thread sanitizer on MacOS as well.
    // https://github.com/ziglang/zig/issues/11403
    const enable_tsan =
        if (b.option(
            bool,
            "enable_tsan",
            "Enable the Thread Sanitizer. This option cannot be enabled at " ++
            "the same time as the Address Sanitizer, and is not supported " ++
            "at all on Windows. Default=false",
        )) |opt|
            opt
        else
            false;

    const enable_ubsan =
        if (b.option(
            bool,
            "enable_ubsan",
            "Enable the Undefined Behavior Sanitizer. Default=true",
        )) |opt|
            opt
        else
            true;

    const jit_mode_raw = b.option(
        []const u8,
        "jit_mode",
        "Which JIT should be used. Default=LLVMv10",
    )
        orelse "LLVMv10";

    const default_symbol_visibility = std.meta.stringToEnum(SymbolVisibility, default_symbol_visibility_raw)
        orelse return error.InvalidSymbolVisibility;
    const jit_mode = std.meta.stringToEnum(JIT_Mode, jit_mode_raw)
        orelse return error.InvalidJitMode;

    switch (jit_mode) {
        .Subzero => switch (target.result.cpu.arch) {
            .x86, .x86_64, .arm, .mipsel => {},
            else => return error.UnsupportedCPUArchitecture,
        },
        else => {},
    }
    if (enable_tsan and target.result.os.tag == .windows) {
        return error.ThreadSanitizerUnsupportedOnWindows;
    } else if (enable_asan and enable_tsan) {
        return error.InvalidOptionPair;
    }

    const swiftshader_dep = b.dependency("swiftshader", .{});
    const spirv_tools_zigbuild_dep = b.dependency("spirv_tools_zigbuild", .{
        .target = target,
        .optimize = optimize,
        .enable_ubsan = enable_ubsan,
        .spirv_headers_path = swiftshader_dep.path("third_party/SPIRV-Headers").getPath(b),
        .spirv_tools_path = swiftshader_dep.path("third_party/SPIRV-Tools").getPath(b),
        .spirv_tools_git_hash_override = @as([]const u8, "dd4b663e13c07fea4fbb3f70c1c91c86731099f7"),
    });
    const xcode_lazy_dep = switch (target.result.os.tag.isDarwin()) {
        true => b.lazyDependency("xcode_frameworks", .{ .target = target, .optimize = optimize }),
        else => null,
    };

    const spirv_tools = spirv_tools_zigbuild_dep.artifact("SPIRV-Tools");

    const cpp_flags = blk: {
        var cpp_flags = std.ArrayList([]const u8).init(b.allocator);

        if (disable_cpp_exceptions) {
            try cpp_flags.append("-fno-exceptions");
        }

        break :blk cpp_flags;
    };
    defer cpp_flags.deinit();

    const default_flags = blk: {
        var default_flags = std.ArrayList([]const u8).init(b.allocator);

        try default_flags.append(switch (default_symbol_visibility) {
            .default => "-fvisibility=default",
            .hidden => "-fvisibility=hidden",
            .internal => "-fvisibility=internal",
            .protected => "-fvisibility=protected",
        });

        if (enable_asan) {
            try default_flags.append("-fsanitize=address");
        }

        if (enable_tsan) {
            try default_flags.append("-fsanitize=thread");
        }

        if (!enable_ubsan) {
            try default_flags.append("-fno-sanitize=undefined");
        }

        try default_flags.appendSlice(&.{
            "-Wall",
            "-Wextra",
        });

        break :blk default_flags;
    };
    defer default_flags.deinit();

    const marl = try build_marl(b, target, optimize, cpp_flags.items, default_flags.items, swiftshader_dep);
    const reactor = switch (jit_mode) {
        .LLVMv10, .LLVMv16 => try build_llvm_reactor(
            b,
            target,
            optimize,
            cpp_flags.items,
            default_flags.items,
            swiftshader_dep,
            jit_mode,
        ),
        .Subzero => try build_subzero_reactor(
            b,
            target,
            optimize,
            cpp_flags.items,
            default_flags.items,
            swiftshader_dep,
            marl,
        ),
    };
    const astc_encoder = try build_astc_encoder(
        b,
        target,
        optimize,
        cpp_flags.items,
        default_flags.items,
        swiftshader_dep,
    );
    const vk_system = try build_vk_system(
        b,
        target,
        optimize,
        cpp_flags.items,
        default_flags.items,
        swiftshader_dep,
        marl,
    );
    const vk_pipeline = try build_vk_pipeline(
        b,
        target,
        optimize,
        cpp_flags.items,
        default_flags.items,
        swiftshader_dep,
        marl,
        reactor,
        spirv_tools,
        vk_system,
    );
    const vk_wsi = try build_vk_wsi(
        b,
        target,
        optimize,
        cpp_flags.items,
        default_flags.items,
        swiftshader_dep,
        xcode_lazy_dep,
        marl,
        reactor,
        spirv_tools,
        vk_pipeline,
        vk_system,
    );
    const vk_device = try build_vk_device(
        b,
        target,
        optimize,
        cpp_flags.items,
        default_flags.items,
        swiftshader_dep,
        astc_encoder,
        marl,
        reactor,
        spirv_tools,
        vk_pipeline,
        vk_system,
    );

    const vk_swiftshader_dynamic = try build_vk_swiftshader(
        b,
        "vk_swiftshader",
        .dynamic,
        target,
        optimize,
        cpp_flags.items,
        default_flags.items,
        swiftshader_dep,
        xcode_lazy_dep,
        astc_encoder,
        marl,
        reactor,
        spirv_tools,
        vk_device,
        vk_pipeline,
        vk_system,
        vk_wsi,
    );
    const vk_swiftshader_static = try build_vk_swiftshader(
        b,
        "vk_swiftshader_static",
        .static,
        target,
        optimize,
        cpp_flags.items,
        default_flags.items,
        swiftshader_dep,
        xcode_lazy_dep,
        astc_encoder,
        marl,
        reactor,
        spirv_tools,
        vk_device,
        vk_pipeline,
        vk_system,
        vk_wsi,
    );

    // This declares intent for the executable to be installed into the
    // standard location when the user invokes the "install" step (the default
    // step when running `zig build`).
    b.installArtifact(vk_swiftshader_static);

    const icd_json = generate_icd_json(
        b,
        b.path("src/vk_swiftshader_icd.json.tmpl"),
        vk_swiftshader_dynamic.getEmittedBin(),
    );
    const icd_json_named_wf = b.addNamedWriteFiles("icd_json");
    _ = icd_json_named_wf.addCopyFile(icd_json, "vk_swiftshader_icd.json");
    const install_icd_json = b.addInstallLibFile(icd_json, "vk_swiftshader_icd.json");
    const install_dynamic = b.addInstallArtifact(vk_swiftshader_dynamic, .{});
    install_dynamic.step.dependOn(&install_icd_json.step);
    b.getInstallStep().dependOn(&install_dynamic.step);
}
