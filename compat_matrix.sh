#!/bin/bash

set -e

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

printf "Subzero JIT\n"
declare -a targets_subzero=(
    # linux
    "x86_64-linux-gnu"
    "x86_64-linux-gnu.2.17"
    "x86_64-linux-musl"
    "x86-linux-gnu"
    "x86-linux-gnu.2.17"
    "x86-linux-musl"
    "arm-linux-gnueabihf"
    "arm-linux-gnueabihf.2.17"
    "arm-linux-musleabihf"
    "mipsel-linux-gnueabihf"
    "mipsel-linux-musl"

    # windows
    "x86_64-windows-gnu"
    "x86-windows-gnu"

    # macos
    "x86_64-macos-none"
)

for t in "${targets_subzero[@]}"
do
    printf "zig build -D\"target=$t\" -D\"optimize=ReleaseSmall\" -D\"jit_mode=Subzero\"\n"
    zig build -D"target=$t" -D"optimize=ReleaseSmall" -D"jit_mode=Subzero"
done

printf "\nLLVMv10 JIT\n"
declare -a targets_llvm10=(
    # linux
    "x86_64-linux-gnu"
    "x86_64-linux-gnu.2.17"
    "x86_64-linux-musl"
    "x86-linux-gnu"
    "x86-linux-gnu.2.17"
    "x86-linux-musl"
    "aarch64-linux-gnu"
    "aarch64-linux-gnu.2.17"
    "aarch64-linux-musl"
    "arm-linux-gnueabihf"
    "arm-linux-gnueabihf.2.17"
    "arm-linux-musleabihf"
    "mips64el-linux-gnuabi64"
    "mips64el-linux-musl"
    "mipsel-linux-gnueabihf"
    "mipsel-linux-musl"

    # windows
    "x86_64-windows-gnu"
    "x86-windows-gnu"
    "aarch64-windows-gnu"

    # macos
    "x86_64-macos-none"
    "aarch64-macos-none"
)

for t in "${targets_llvm10[@]}"
do
    printf "zig build -D\"target=$t\" -D\"optimize=ReleaseSmall\" -D\"jit_mode=LLVMv10\"\n"
    zig build -D"target=$t" -D"optimize=ReleaseSmall" -D"jit_mode=LLVMv10"
done

printf "\nLLVMv16 JIT\n"
declare -a targets_llvm16=(
    # linux
    "x86_64-linux-gnu"
    "x86_64-linux-gnu.2.17"
    "x86_64-linux-musl"
    "x86-linux-gnu"
    "x86-linux-gnu.2.17"
    "x86-linux-musl"
    "aarch64-linux-gnu"
    "aarch64-linux-gnu.2.17"
    "aarch64-linux-musl"
    "arm-linux-gnueabihf"
    "arm-linux-gnueabihf.2.17"
    "arm-linux-musleabihf"
    # https://github.com/ziglang/zig/issues/3340
    # "riscv64-linux-gnu"
    # "riscv64-linux-gnu.2.27"
    "riscv64-linux-musl"
    "mips64el-linux-gnuabi64"
    "mips64el-linux-musl"
    "mipsel-linux-gnueabihf"
    "mipsel-linux-musl"

    # windows
    "x86_64-windows-gnu"
    "x86-windows-gnu"
    "aarch64-windows-gnu"

    # macos
    "x86_64-macos-none"
    "aarch64-macos-none"
)

for t in "${targets_llvm16[@]}"
do
    printf "zig build -D\"target=$t\" -D\"optimize=ReleaseSmall\" -D\"jit_mode=LLVMv16\"\n"
    zig build -D"target=$t" -D"optimize=ReleaseSmall" -D"jit_mode=LLVMv16"
done
