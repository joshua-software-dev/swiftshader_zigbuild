const std = @import("std");


fn replace(buffer: *std.ArrayList(u8), search_for: []const u8, replace_with: []const u8) !void {
    if (std.mem.indexOf(u8, buffer.items, search_for)) |pos| {
        try buffer.replaceRange(pos, search_for.len, replace_with);
        return;
    }

    return error.NotFound;
}

fn write_buffer_to_file(cwd: std.fs.Dir, output_file_path: []const u8, buffer: []const u8) !void {
    const output_dir_path = std.fs.path.dirname(output_file_path)
        orelse return error.InvalidOutputPath;
    const output_filename = std.fs.path.basename(output_file_path);

    // cwd.openFile accepts relative and absolute paths
    const output_file = cwd.openFile(output_file_path, .{ .mode = .write_only })
        catch |err| switch (err) {
            error.FileNotFound => blk: {
                var output_dir = try cwd.makeOpenPath(output_dir_path, .{});
                defer output_dir.close();
                break :blk try output_dir.createFile(output_filename, .{ .mode = 0o644 });
            },
            else => return err,
        };
    defer output_file.close();

    try output_file.writeAll(buffer);
    try output_file.setEndPos(buffer.len);
}

pub fn main() !u8 {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    if (args.len != 4) {
        std.debug.print("usage: {s} <template-file-path> <output-file-path> <replacement-path>", .{ args[0] });
        return 1;
    }

    const template_file_path = args[1];
    const output_file_path = args[2];
    const replacement_path = std.fs.path.basename(args[3]);

    const cwd = std.fs.cwd();
    var header_buffer = blk: {
        const header_file = try cwd.openFile(template_file_path, .{});
        defer header_file.close();

        var header_buffer = std.ArrayList(u8).init(allocator);
        try header_file.reader().readAllArrayList(&header_buffer, std.math.maxInt(usize));
        break :blk header_buffer;
    };
    defer header_buffer.deinit();

    try replace(
        &header_buffer,
        "${ICD_LIBRARY_PATH}",
        "${ICD_LIBRARY_PATH1}${ICD_LIBRARY_PATH2}",
    );
    try replace(
        &header_buffer,
        "${ICD_LIBRARY_PATH1}",
        "./",
    );
    try replace(
        &header_buffer,
        "${ICD_LIBRARY_PATH2}",
        replacement_path,
    );

    try write_buffer_to_file(cwd, output_file_path, header_buffer.items);

    return 0;
}
