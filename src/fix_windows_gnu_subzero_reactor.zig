const std = @import("std");


fn write_buffer_to_file(cwd: std.fs.Dir, output_file_path: []const u8, buffer: []const u8) !void {
    const output_dir_path = std.fs.path.dirname(output_file_path)
        orelse return error.InvalidOutputPath;
    const output_filename = std.fs.path.basename(output_file_path);

    // cwd.openFile accepts relative and absolute paths
    const output_file = cwd.openFile(output_file_path, .{ .mode = .write_only })
        catch |err| switch (err) {
            error.FileNotFound => blk: {
                var output_dir = try cwd.makeOpenPath(output_dir_path, .{});
                defer output_dir.close();
                break :blk try output_dir.createFile(output_filename, .{ .mode = 0o644 });
            },
            else => return err,
        };
    defer output_file.close();

    try output_file.writeAll(buffer);
    try output_file.setEndPos(buffer.len);
}

pub fn main() !u8 {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    if (args.len != 3) {
        std.debug.print("usage: {s} <source-file-path> <output-path>", .{ args[0] });
        return 1;
    }

    const source_file_path = args[1];
    const output_file_path = args[2];

    const cwd = std.fs.cwd();
    var source_buffer = blk: {
        const source_file = try cwd.openFile(source_file_path, .{});
        defer source_file.close();

        var source_buffer = std.ArrayList(u8).init(allocator);
        try source_file.reader().readAllArrayList(&source_buffer, std.math.maxInt(usize));
        break :blk source_buffer;
    };
    defer source_buffer.deinit();

    try source_buffer.insertSlice(0, "#include <intrin.h>\n");

    try write_buffer_to_file(cwd, output_file_path, source_buffer.items);

    return 0;
}
